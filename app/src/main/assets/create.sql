CREATE TABLE [notification] (
	[id] Integer PRIMARY KEY,
	[title] nvarchar(50),
	[message] nvarchar(50),
	[categoryItem] nvarchar(50),
	[FbId] int NULL,
	[Gmail] nvarchar(50) NULL,
	[status] default 0,
	[favourite] default 0,
	[time] datetime NULL,
	[flag] Integer

);

CREATE TABLE [userinfo] (
	[id] Integer PRIMARY KEY,
	[FbProfileName] nvarchar(50) NULL,
	[FbId] int NULL,
	[GmailProfileName] nvarchar(50) NULL,
	[Gmail] nvarchar(50) NULL,
	[Ucategory] nvarchar(50) NULL,
	[fbphotoUrl] nvarchar(300) NULL
);

CREATE TABLE [archive] (
	[id] Integer PRIMARY KEY,
	[title] nvarchar(50),
	[message] nvarchar(50),
	[time] datetime NULL
);

CREATE TABLE [category] (
	[id] Integer PRIMARY KEY,
	[category] nvarchar(50),
	[cflag] default 0
);
