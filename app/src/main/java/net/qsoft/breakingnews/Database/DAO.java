package net.qsoft.breakingnews.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import net.qsoft.breakingnews.Model.ActiveUser;
import net.qsoft.breakingnews.Model.Category;
import net.qsoft.breakingnews.app.AppController;
import net.qsoft.breakingnews.Model.Message;

import java.util.ArrayList;

import static net.qsoft.breakingnews.Activity.GmailActivity.email;
import static net.qsoft.breakingnews.Activity.GmailActivity.fb_id;
import static net.qsoft.breakingnews.Database.DBHelper.TABLE_ARCHIVE;
import static net.qsoft.breakingnews.Database.DBHelper.TABLE_CATEGORY;
import static net.qsoft.breakingnews.Database.DBHelper.TABLE_NOTIFICATION;

public class DAO {
    private static final String TAG = DAO.class.getSimpleName();

    // Database fields
    private SQLiteDatabase db;
    private DBHelper dbHelper;

    public DAO(Context context) {
        dbHelper = new DBHelper(context);
    }

    protected void finalize() throws Throwable {
        // TODO Auto-generated method stub
        if (db != null && db.isOpen())
            db.close();
    }

    public void open() throws SQLException {
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Cursor getRecordsCursor(String sql, String[] param) {
        Cursor curs = null;
        curs = db.rawQuery(sql, param);
        return curs;
    }


    public void execSQL(String sql, String[] param) throws SQLException {
        db.execSQL(sql, param);
    }

    public static void executeSQL(String sql, String[] param) {
        DAO da = new DAO(AppController.getInstance());
        da.open();
        try {
            da.execSQL(sql, param);
        } catch (Exception e) {
            throw e;
        } finally {
            da.close();
        }
    }


    public ArrayList<Message> getAllArchiveMessage() {
        ArrayList<Message> messagesArrayList = new ArrayList<Message>();
        Message message = null;
        Cursor curs = null;

        try {
//			curs = db.query("SELECT " + TABLE_NOTIFICATION + " WHERE flag= "+Message.FLAG_FOR_FAVOURITE,new String[] { "[id]", "[title]",
//					"[message]","[status]", "[flag]"}, null, null, null, null, null);
//

            curs = db.query(TABLE_NOTIFICATION + " WHERE flag=" + Message.FLAG_FOR_FAVOURITE, new String[]{"[id]", "[title]",
                    "[message]", "[categoryItem]", "[status]", "[flag]","[time]", "[favourite]"}, null, null, null, null, null);

            if (curs.moveToFirst()) {
                do {
                    message = new Message(curs.getString(0), curs.getString(1), curs.getString(2),
                            curs.getString(3), curs.getInt(4), curs.getInt(5),
                            curs.getString(6),curs.getInt(7));
                    messagesArrayList.add(message);
                } while (curs.moveToNext());
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return messagesArrayList;
    }

    public ArrayList<Message> getAllArchive(String c) {
        ArrayList<Message> messagesArrayList = new ArrayList<>();
        ArrayList<Category> categoryArrayList = new ArrayList<>();

        Cursor curs = null;
        String selectQuery = null;
        String c3;

        selectQuery= "SELECT * FROM notification WHERE categoryItem = '"+ c+"'";


//        for (int j = 0; j < getAllCategory().size(); j++) {
//            c3 = getAllCategory().get(j).getCategory();
//            if (c3.equals(c)) {
//               selectQuery= "SELECT * FROM notification WHERE categoryItem = '"+ c3+"'";
//            }
//        }

//        switch (c) {
//            case "Traffic":
//                selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION + " WHERE categoryItem=" + "'Traffic'";
//                break;
//            case "Sports":
//                selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION + " WHERE categoryItem=" + "'Sports'";
//                break;
//            case "Politics":
//                selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION + " WHERE categoryItem=" + "'Politics'";
//                break;
//            case "Fitness":
//                selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION + " WHERE categoryItem=" + "'Fitness'";
//                break;
//            case "Jobs":
//                 selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION + " WHERE categoryItem=" + "'Jobs'";
//                break;
//            case "Economics":
//                selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION + " WHERE categoryItem=" + "'Economics'";
//                break;
//            case "Entertainment":
//                selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION + " WHERE categoryItem=" + "'Entertainment'";
//                break;
//            case "International":
//                selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION + " WHERE categoryItem=" + "'International'";
//                break;
//            case "Life & Style":
//                selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION + " WHERE categoryItem=" + "'Life style'";
//                break;
//            case "Business":
//                selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION + " WHERE categoryItem=" + "'Business'";
//                break;
//            case "Tech News":
//                selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION + " WHERE categoryItem=" + "'Tech News'";
//                break;
//            case "Education":
//                selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION + " WHERE categoryItem=" + "'Education'";
//            case "Regional":
//                selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION + " WHERE categoryItem=" + "'Regional'";
//            case "Social":
//                selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION + " WHERE categoryItem=" + "'Social'";
//            case "TV":
//                selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION + " WHERE categoryItem=" + "'TV'";
//
//            case "Opinion":
//                selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION + " WHERE categoryItem=" + "'Opinion'";
//
//            case "Editorials":
//                selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION + " WHERE categoryItem=" + "'Editorials'";
//            case "Services":
//                selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION + " WHERE categoryItem=" + "'Services'";
//            case "Spirituality":
//                selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION + " WHERE categoryItem=" + "'Spirituality'";
//                break;
//        }

        curs = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (curs.moveToFirst()) {
            do {
                Message message = new Message();
                message.setId(curs.getString(0));
                message.setTitle(curs.getString(1));
                message.setMessage(curs.getString(2));
                message.setCategory(curs.getString(3));
                message.setStatus(curs.getInt(4));
                message.setFlag(curs.getInt(5));
                message.setTime(curs.getString(6));
                message.setFavourite(curs.getInt(7));

                // Adding message to list
                messagesArrayList.add(message);
            } while (curs.moveToNext());
        }

        // return message list
        return messagesArrayList;

    }


    public ArrayList<Message> getAllCategoryGroupMessage(String c) {
        ArrayList<Message> messageArrayList = new ArrayList<Message>();
        Message message = null;
        Cursor curs = null;

        try {
//            		curs = db.query("SELECT " + TABLE_NOTIFICATION + " WHERE categoryItem= "+"Sports", new String[] { "[id]", "[title]",
//					"[message]","[categoryItem]", "[status]", "[flag]", "favourite"}, null, null, null, null, null);


            curs = db.query(TABLE_NOTIFICATION + " WHERE categoryItem = '"+ c+"'", new String[]{"[id]", "[title]",
                    "[message]", "[categoryItem]", "[status]", "[flag]","[time]", "[favourite]"}, null, null, null, null, null);

            if (curs.moveToFirst()) {
                do {
                    message = new Message(curs.getString(0), curs.getString(1), curs.getString(2),
                            curs.getString(3), curs.getInt(4), curs.getInt(5),
                            curs.getString(6),curs.getInt(7));
                    messageArrayList.add(message);
                } while (curs.moveToNext());
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return messageArrayList;
    }


    public ArrayList<Message> getAllMessage() {
        ArrayList<Message> messageArrayList = new ArrayList<Message>();
        Message message = null;
        Cursor curs = null;

        try {
            curs = db.query(TABLE_NOTIFICATION + " ORDER BY id DESC", new String[]{"[id]", "[title]",
                    "[message]", "[categoryItem]", "[status]", "[flag]","[time]", "[favourite]"}, null, null, null, null, null);

            if (curs.moveToFirst()) {
                do {
                    message = new Message(curs.getString(0), curs.getString(1), curs.getString(2),
                            curs.getString(3), curs.getInt(4), curs.getInt(5),
                            curs.getString(6),curs.getInt(7));
                    messageArrayList.add(message);
                } while (curs.moveToNext());
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return messageArrayList;
    }

    public ActiveUser getActiveUser(ActiveUser usr) {
        Cursor curs = null;

        try {
            curs = db.query(DBHelper.TABLE_USERINFO, new String[]{"FbProfileName",
                            "FbId", "GmailProfileName", "Gmail", "Ucategory",
                            "fbphotoUrl"}, null, null, null,
                    null, null);

            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                usr.setFbProfileName(curs.getString(0));
                usr.setFbId(curs.getLong(1));
                usr.setGmailProfileName(curs.getString(2));
                usr.setGmail(curs.getString(3));
                usr.setUcategory(curs.getString(4));
                usr.setPhotoUrl(curs.getString(5));
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }
        return usr;
    }

    public ArrayList<Message> gettingAllDraftMessage() {
        ArrayList<Message> messageArrayList = new ArrayList<Message>();
        Message message = null;
        Cursor curs = null;

        try {
            curs = db.query(TABLE_ARCHIVE + " ORDER BY id DESC", new String[]{"[id]", "[title]",
                    "[message]", "[time]"}, null, null, null, null, null);

            if (curs.moveToFirst()) {
                do {
                    message = new Message(curs.getString(0), curs.getString(1),
                            curs.getString(2), curs.getString(3));
                    messageArrayList.add(message);
                } while (curs.moveToNext());
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return messageArrayList;
    }

    public boolean messageExists(String id) {
        boolean found = false;


        String selectQuery = "SELECT * FROM " + TABLE_CATEGORY + " WHERE id=" + id;
        Cursor curs = db.rawQuery(selectQuery, null);

        found = !(!curs.moveToFirst() || curs.getCount() == 0);

        curs.close();

        return found;
    }

    // Deleting All Message
    public void deleteAllMessage() {
        db.delete(TABLE_NOTIFICATION, null, null);
        db.close();
    }

    // Deleting A Client
    public void deleteSingleMessage(String id) {
        db.delete(TABLE_NOTIFICATION, "[id]=?",
                new String[]{id});
        //db.delete(TABLE_NOTIFICATION, id + " LIKE ? ", new String[]{"%" + id + "%"});
        db.close();
    }

    // Deleting A Client
    public void deleteSingleCategory(String id) {
        db.delete(TABLE_CATEGORY, "[id]=?",
                new String[]{id});
        //db.delete(TABLE_NOTIFICATION, id + " LIKE ? ", new String[]{"%" + id + "%"});
        db.close();
    }

    public void UpdateSingleCategoryFlag(int cflag, String id) {
        ContentValues cv = new ContentValues();

        cv.clear();
        cv.put("[cflag]", cflag);

        db.update(TABLE_CATEGORY, cv, "[id]=?",
                new String[]{id});

        //db.update(TABLE_NOTIFICATION, cv, null, null);
    }

    public void UpdateStatus(int status, String id) {
        ContentValues cv = new ContentValues();

        cv.clear();
        cv.put("[status]", status);

        db.update(TABLE_NOTIFICATION, cv, "[id]=?",
                new String[]{id});

        //db.update(TABLE_NOTIFICATION, cv, null, null);
    }

    public void UpdateFlag(int flag, String id) {
        ContentValues cv = new ContentValues();

        cv.clear();
        cv.put("[flag]", flag);

        db.update(TABLE_NOTIFICATION, cv, "[id]=?",
                new String[]{id});

        //db.update(TABLE_NOTIFICATION, cv, null, null);
    }

    public void UpdateFavourite(int favourite, String id) {
        ContentValues cv = new ContentValues();

        cv.clear();
        cv.put("[favourite]", favourite);

        db.update(TABLE_NOTIFICATION, cv, "[id]=?",
                new String[]{id});

        //db.update(TABLE_NOTIFICATION, cv, null, null);
    }

    public void UpdateCheckedBox(boolean Checked, String id) {
        ContentValues cv = new ContentValues();

        cv.clear();
        cv.put("[Checked]", Checked);

        db.update(TABLE_CATEGORY, cv, "[id]=?",
                new String[]{id});

        //db.update(TABLE_NOTIFICATION, cv, null, null);
    }


    public ArrayList<Message> getAllReadMessage() {
        ArrayList<Message> messagesArrayList = new ArrayList<Message>();
        Message message = null;
        Cursor curs = null;

        try {
//			curs = db.query("SELECT " + TABLE_NOTIFICATION + " WHERE flag= "+Message.FLAG_FOR_FAVOURITE,new String[] { "[id]", "[title]",
//					"[message]","[status]", "[flag]"}, null, null, null, null, null);
//

            curs = db.query(TABLE_NOTIFICATION + " WHERE status=" + Message.READ, new String[]{"[id]", "[title]",
                    "[message]", "[categoryItem]", "[status]", "[flag]","[time]", "[favourite]"}, null, null, null, null, null);

            if (curs.moveToFirst()) {
                do {
                    message = new Message(curs.getString(0), curs.getString(1), curs.getString(2),
                            curs.getString(3), curs.getInt(4), curs.getInt(5),
                            curs.getString(6),curs.getInt(7));
                    messagesArrayList.add(message);
                } while (curs.moveToNext());
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return messagesArrayList;
    }

    public ArrayList<Message> getAllUnReadMessage() {
        ArrayList<Message> messagesArrayList = new ArrayList<Message>();
        Message message = null;
        Cursor curs = null;

        try {
//			curs = db.query("SELECT " + TABLE_NOTIFICATION + " WHERE flag= "+Message.FLAG_FOR_FAVOURITE,new String[] { "[id]", "[title]",
//					"[message]","[status]", "[flag]"}, null, null, null, null, null);
//

            curs = db.query(TABLE_NOTIFICATION + " WHERE status=" + Message.UNREAD, new String[]{"[id]", "[title]",
                    "[message]", "[categoryItem]", "[status]", "[flag]","[time]", "[favourite]"}, null, null, null, null, null);

            if (curs.moveToFirst()) {
                do {
                    message = new Message(curs.getString(0), curs.getString(1), curs.getString(2),
                            curs.getString(3), curs.getInt(4), curs.getInt(5),
                            curs.getString(6),curs.getInt(7));
                    messagesArrayList.add(message);
                } while (curs.moveToNext());
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return messagesArrayList;
    }


    public ArrayList<Category> getAllSelectedCategory() {
        ArrayList<Category> categoryArrayList = new ArrayList<Category>();
        Category category = null;
        Cursor curs = null;

        try {

            curs = db.query(TABLE_CATEGORY , new String[]{"[id]", "[category]", "[cflag]"}, null, null, null, null, null);


          ///  curs = db.query(TABLE_CATEGORY + " WHERE cflag=" + Category.FLAG_FOR_CATEGORY, new String[]{"[id]", "[category]", "[cflag]"}, null, null, null, null, null);

            if (curs.moveToFirst()) {
                do {
                    category = new Category(curs.getString(0), curs.getString(1), curs.getInt(2));
                    categoryArrayList.add(category);
                } while (curs.moveToNext());
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return categoryArrayList;
    }


    public ArrayList<Category> getAllCategory() {
        ArrayList<Category> categoryArrayList = new ArrayList<Category>();
        Category category = null;
        Cursor curs = null;

        try {


            curs = db.query(TABLE_CATEGORY, new String[]{"[id]", "[category]", "[cflag]"}, null, null, null, null, null);

            if (curs.moveToFirst()) {
                do {
                    category = new Category(curs.getString(0), curs.getString(1), curs.getInt(2));
                    categoryArrayList.add(category);
                } while (curs.moveToNext());
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return categoryArrayList;
    }

    // Getting single Category----------
    public Message getSingleCategory() {
        Message m1 = null;
        Cursor curs = null;

        try {
//            curs = db.query(TABLE_NOTIFICATION, new String[]{"[id]", "[category]", "[cflag]"}, null, null, null, null, null);
//
//            curs.moveToFirst();
//            if (!curs.isAfterLast()) {
//                cl = new Category(curs.getString(0), curs.getString(1),
//                        curs.getInt(2));
//            }


            curs = db.query(TABLE_NOTIFICATION , new String[]{"[id]", "[title]",
                    "[message]", "[categoryItem]", "[status]", "[flag]", "[favourite]"}, null, null, null, null, null);
            curs.moveToFirst();
            if (!curs.isAfterLast()) {

                    m1 = new Message(curs.getString(0), curs.getString(1), curs.getString(2),
                            curs.getString(3), curs.getInt(4), curs.getInt(5), curs.getInt(6));
                }


        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }
        return m1;

    }


    public void UpdateCategoryItem(String id, Category category) {
        ContentValues cv = new ContentValues();

        cv.clear();
        cv.put("[category]", category.getCategory());
        cv.put("[cflag]", category.getCflag());


        db.update(TABLE_CATEGORY, cv, "[id]=?",
                new String[]{id});

    }
}
