package net.qsoft.breakingnews.app;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.facebook.appevents.AppEventsLogger;

import io.fabric.sdk.android.Fabric;
import net.qsoft.breakingnews.Activity.MainActivity;
import net.qsoft.breakingnews.BuildConfig;
import net.qsoft.breakingnews.NetworkService.LruBitmapCache;
import net.qsoft.breakingnews.R;

import java.lang.reflect.Method;

import static net.qsoft.breakingnews.Activity.GmailActivity.email;
import static net.qsoft.breakingnews.Activity.GmailActivity.fb_id;
import static net.qsoft.breakingnews.Activity.GmailActivity.fb_profile_name;
import static net.qsoft.breakingnews.Activity.GmailActivity.fbphotoUrl;
import static net.qsoft.breakingnews.Activity.GmailActivity.personName;
import static net.qsoft.breakingnews.Activity.GmailActivity.personPhotoUrl;
import static net.qsoft.breakingnews.Service.NotificationService.MyPREFERENCES;


public class AppController extends Application {
    public static final String defaultImageUrl = "https://t3.ftcdn.net/jpg/01/83/55/76/240_F_183557656_DRcvOesmfDl5BIyhPKrcWANFKy2964i9.jpg";

    public static final String TAG = AppController.class.getSimpleName();

    public static final String BaseUrl = "http://qsoft.net/khobor/public/";
    public static final String UserInfoUrl = "userinformation";
    public static final String GetCategoryUrl = "categorydata";
    public static final String SendCategoryToserverUrl = "userinfocategory";

    public static final String UserUpdateCategoryUrl = "userupdatecategory";
    public static final String GetNotificationUrl = "notificationdata";
    public static final String NotifyDataDelete = "delete";

    private static Context sContext;

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    private static AppController mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mInstance = this;
        AppEventsLogger.activateApp(this);
        sContext=   getApplicationContext();

    }


    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public static Context getContext() {
        return sContext;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }


    public static final String getUserInfoUrl() {
        return BaseUrl + UserInfoUrl;
    }

    public static final String getUserUpdateCategoryUrl() {
        return BaseUrl + UserUpdateCategoryUrl;
    }

    public static final String getSendCategoryToserverUrl() {
        return BaseUrl + SendCategoryToserverUrl;
    }

    public static final String getGetCategoryUrl() {
        return BaseUrl +GetCategoryUrl ;
    }

    public static final String getGetNotificationUrl() {
        return BaseUrl + GetNotificationUrl;
    }

       public static final String getNotifyDataDelete() {
        return BaseUrl + NotifyDataDelete;
    }


    public static void showDebugDBAddressLogToast(Context context) {
        if (BuildConfig.DEBUG) {
            try {
                Class<?> debugDB = Class.forName("com.amitshekhar.DebugDB");
                Method getAddressLog = debugDB.getMethod("getAddressLog");
                Object value = getAddressLog.invoke(null);
                Toast.makeText(context, (String) value, Toast.LENGTH_LONG).show();
            } catch (Exception ignore) {

            }
        }
    }

}
