package net.qsoft.breakingnews.Service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import net.qsoft.breakingnews.Activity.MainActivity;
import net.qsoft.breakingnews.Activity.MessageActivity;
import net.qsoft.breakingnews.Adapter.CategorySelectAdapter;
import net.qsoft.breakingnews.Adapter.MessageListAdapter;
import net.qsoft.breakingnews.Database.DAO;
import net.qsoft.breakingnews.Model.Category;
import net.qsoft.breakingnews.Model.Message;
import net.qsoft.breakingnews.NetworkService.Utils;
import net.qsoft.breakingnews.NetworkService.VolleyCustomRequest;
import net.qsoft.breakingnews.R;
import net.qsoft.breakingnews.app.AppController;
import net.qsoft.breakingnews.utils.CloudRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.android.volley.VolleyLog.v;
import static net.qsoft.breakingnews.Activity.GmailActivity.email;
import static net.qsoft.breakingnews.Activity.GmailActivity.fb_id;
import static net.qsoft.breakingnews.Database.DBHelper.TABLE_CATEGORY;
import static net.qsoft.breakingnews.Database.DBHelper.TABLE_NOTIFICATION;

public class NotificationService extends Service {
    private static int flag = 0;
    private static final String TAG = NotificationService.class.getSimpleName();
    private MessageListAdapter adapter;
    private CategorySelectAdapter adapter1;

    public static int sum = 1;
    private int id;
    public ArrayList<Message> messagesList;
    ArrayList<Category> categoryArrayList;

    private String mbody, title, category, time, exist, cid, Selectcategory, Cid;
    private RequestQueue mRequestQueue;

    public static SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";

    public NotificationService() {

    }

    Intent intent;

    @Override
    public int onStartCommand(Intent intent, final int flags, int startId) {

        if (Utils.isNetworkAvailable(this)) {
            getNotifications();
            GetCategory();
        }

        return Service.START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startForeground(1, new Notification());
    }


    public void getNotifications() {
        messagesList = new ArrayList<Message>();
        adapter = new MessageListAdapter(getApplicationContext(), messagesList);

        // String url = "https://bytelab.000webhostapp.com/Product/notification.php?";
        //String url = "http://192.168.0.117/JeneRakhun/public/notificationdata";
        // String url = "http://qsoft.net/khobor/public/notificationdata";
        //String url = "http://192.168.0.17:8080/khobor/public/notificationdata";

        String url = AppController.getGetNotificationUrl();


        //    RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());

//        if (mRequestQueue == null) {
//            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
//        }

        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, "Notify server Response: " + response.toString());

                        try {
                            v("Response:%n %s", response.toString(4));
                            DAO dao = new DAO(getApplicationContext());
                            dao.open();
                            // exist = String.valueOf(dao.messageExists("id"));
                            // if (exist.equals("id")) {

                            for (int i = 0; i < response.length(); i++) {

                                JSONObject notity = (JSONObject) response
                                        .get(i);

                                Message message = new Message();
                                id = Integer.parseInt(message.setId(notity.getString("id")));
                                title = message.setTitle(notity.getString("Title"));
                                mbody = message.setMessage(notity.getString("Description"));
                                category = message.setCategory(notity.getString("Category"));
                                time = message.setCategory(notity.getString("Time"));


                                messagesList.add(message);
                                // int status = Integer.parseInt(notity.getString("status"));

                                DAO.executeSQL("INSERT OR REPLACE INTO " + TABLE_NOTIFICATION + "(id, title, message, categoryItem, FbId, Gmail, time) " +
                                                "VALUES(?, ?, ?, ?, ?, ?, ?)",

                                        new String[]{String.valueOf(id),
                                                title, mbody, category, email, String.valueOf(fb_id), time});


                                sharedpreferences = getApplicationContext().getSharedPreferences(
                                        MyPREFERENCES, 0);
                                flag = sharedpreferences.getInt("f", flag);

                                if (flag != notity.getInt("id")) {
                                    //notifyUser(category, mbody, sum);

                                    //------------------------------------------

                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        showNotification(getApplicationContext(), category, mbody, sum);
                                    } else {
                                        notifyUser(category, mbody, sum);
                                        sum = sum + 1;
                                        UpdateStatus();
                                    }

                                    //-----------------------------------------


                                    flag = notity.getInt("id");
                                    sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedpreferences.edit();
                                    editor.putInt("f", flag);
                                    editor.apply();
                                    sum = sum + 1;
                                    editor.putInt("s", sum);
                                    editor.apply();
                                    UpdateStatus();
                                }


                            }

                            // }

                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.i("Volley error:", volleyError.toString());
                Log.d(TAG, "Error: " + volleyError.getMessage());

//                if (volleyError instanceof NetworkError) {
//                    Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();
//                } else if (volleyError instanceof ServerError) {
//                    Toast.makeText(getApplicationContext(), "The server could not be found. Please try again after some time!!", Toast.LENGTH_SHORT).show();
//
//                } else if (volleyError instanceof AuthFailureError) {
//                    Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();
//
//
//                } else if (volleyError instanceof ParseError) {
//                    Toast.makeText(getApplicationContext(), "Parsing error! Please try again after some time!!", Toast.LENGTH_SHORT).show();
//
//                } else if (volleyError instanceof NoConnectionError) {
//                    Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();
//
//                } else if (volleyError instanceof TimeoutError) {
//                    Toast.makeText(getApplicationContext(), "Connection TimeOut! Please check your internet connection", Toast.LENGTH_SHORT).show();
//
//                }
            }
        }) {
            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("FbId", String.valueOf(fb_id));
                headers.put("Gmail", email);
                return headers;
            }

        };

//        int socketTimeout = 50000;//30 seconds - change to what you want
//        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        req.setRetryPolicy(policy);
//        mRequestQueue.add(req);
//
        // Volley.newRequestQueue(this).add(req);

        CloudRequest.getInstance(this).addToRequestQueue(req);

    }

    public void UpdateStatus() {

        //  String hitURL = "https://bytelab.000webhostapp.com/Product/update.php?";
        // String hitURL = "http://192.168.0.117/JeneRakhun/public/delete";
        // String hitURL = "http://qsoft.net/khobor/public/delete";
        // String hitURL = "http://192.168.0.17:8080/khobor/public/delete";
        String hitURL = AppController.getNotifyDataDelete();


        // HashMap<String, String> params = new HashMap<>();
        // params.put("accept", "accept");

        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());

        VolleyCustomRequest req = new VolleyCustomRequest(Request.Method.GET, hitURL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d(TAG, "Update Server Response: " + response.toString());
                        try {
                            Log.v("Response:%n %s", response.toString(4));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.i("Volley error:", volleyError.toString());
                        Log.d(TAG, "Error message: " + volleyError.getMessage());

//                        if (volleyError instanceof NetworkError) {
//                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();
//                        }
//
//                        else if (volleyError instanceof ServerError) {
//                            Toast.makeText(getApplicationContext(), "The server could not be found. Please try again after some time!!", Toast.LENGTH_SHORT).show();
//
//                        } else if (volleyError instanceof AuthFailureError) {
//                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();
//
//
//                        } else if (volleyError instanceof ParseError) {
//                            Toast.makeText(getApplicationContext(), "Parsing error! Please try again after some time!!", Toast.LENGTH_SHORT).show();
//
//                        } else if (volleyError instanceof NoConnectionError) {
//                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();
//
//                        } else if (volleyError instanceof TimeoutError) {
//                            Toast.makeText(getApplicationContext(), "Connection TimeOut! Please check your internet connection", Toast.LENGTH_SHORT).show();
//
//                        }
                    }
                }) {
            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("FbId", String.valueOf(fb_id));
                headers.put("Gmail", email);
                headers.put("accept", "accept");
                return headers;
            }

        };

        int socketTimeout = 50000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        mRequestQueue.add(req);

        // Volley.newRequestQueue(this).add(req);
        CloudRequest.getInstance(this).addToRequestQueue(req);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void notifyUser(String category, String message, int sum) {
        int mId = 001;

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                        .setContentTitle("New news from:" + category)
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setOnlyAlertOnce(true)
                        .setOngoing(true)
                        .setNumber(sum)
                        .setTimeoutAfter(1000)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        mBuilder.setPriority(Notification.PRIORITY_HIGH);
        if (Build.VERSION.SDK_INT >= 21)
            mBuilder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000}).build();

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, MessageActivity.class);
        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (mNotificationManager != null) {
            mNotificationManager.cancelAll();
        }


        // mId allows you to update the notification later on.
        if (mNotificationManager != null) {
            mNotificationManager.notify(mId, mBuilder.build());
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    public void showNotification(Context context, String title, String body, int sum) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        int notificationId = 1;
        String channelId = "channel-01";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(mChannel);
            }
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(body)
                .setNumber(sum);

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, MessageActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        mBuilder.setContentIntent(resultPendingIntent);

        if (notificationManager != null) {
            notificationManager.notify(notificationId, mBuilder.build());
        }
    }


    public String getNotifyTime() {
        DateFormat timeFormat = new SimpleDateFormat("hh:mm a", Locale.getDefault());
        return timeFormat.format(new Date());
    }

    public String getNotifyDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yy", Locale.getDefault());
        return dateFormat.format(new Date());
    }


    public void GetCategory() {

        categoryArrayList = new ArrayList<Category>();
        adapter1 = new CategorySelectAdapter(getApplicationContext(), categoryArrayList);

        // String hitUrl = "http://192.168.0.117/JeneRakhun/public/categorydata";
        //String hitUrl = "http://qsoft.net/khobor/public/categorydata";
        // String hitUrl = "http://192.168.0.17:8080/khobor/public/categorydata";

        String hitUrl = AppController.getGetCategoryUrl();

        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());

        // Creating volley request obj
        JsonArrayRequest categoryReq = new JsonArrayRequest(hitUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, "Json response: " + response.toString());


                        DAO dao = new DAO(getApplicationContext());
                        dao.open();


                        // Parsing json
                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject obj = response.getJSONObject(i);

                                Category category1 = new Category();
                                category1.setId(obj.getString("id"));
                                category1.setCategory(obj.getString("Category"));
                                category1.setCflag(obj.getInt("cflag"));
                                categoryArrayList.add(category1);

                                cid = obj.getString("id");
                                Selectcategory = obj.getString("Category");


//                                     DAO.executeSQL("INSERT OR REPLACE INTO " + TABLE_CATEGORY + "(id, category, cflag) " +
//                                                         "VALUES(?, ?, ?)",
//
//                                                 new String[]{obj.getString("id"), obj.getString("Category"),
//                                                 obj.getString("cflag")});


//                                     sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
//                                     SharedPreferences.Editor editor = sharedpreferences.edit();
//                                     editor.putString("CID", cid);
//                                     editor.apply();
//
//                                     sharedpreferences = getApplicationContext().getSharedPreferences(
//                                             MyPREFERENCES, 0);
//                                      Cid = sharedpreferences.getString("CID", cid);

                                //  if (!Cid.equals(obj.getString("id"))) {

//                                         DAO.executeSQL("INSERT OR REPLACE INTO " + TABLE_CATEGORY + "(id, category) " +
//                                                         "VALUES(?, ?)",
//
//                                                 new String[]{obj.getString("id"), obj.getString("Category")});


                                // }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                        adapter.notifyDataSetChanged();
                        saveIntoDatabase(categoryArrayList);


                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data

                        //  adapter.notifyDataSetChanged();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                VolleyLog.d(TAG, "Error: " + volleyError.getMessage());
                volleyError.printStackTrace();

//                if (volleyError instanceof NetworkError) {
//                    Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();
//                } else if (volleyError instanceof ServerError) {
//                    Toast.makeText(getApplicationContext(), "The server could not be found. Please try again after some time!!", Toast.LENGTH_SHORT).show();
//
//                } else if (volleyError instanceof AuthFailureError) {
//                    Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();
//
//
//                }
//                else if (volleyError instanceof ParseError) {
//                    Toast.makeText(getApplicationContext(), "Parsing error! Please try again after some time!!", Toast.LENGTH_SHORT).show();
//
//                }
//
//                else if (volleyError instanceof NoConnectionError) {
//                    Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();
//
//                } else if (volleyError instanceof TimeoutError) {
//                    Toast.makeText(getApplicationContext(), "Connection TimeOut! Please check your internet connection", Toast.LENGTH_SHORT).show();
//
//                }
            }
        })

        {
            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("FbId", String.valueOf(fb_id));
                headers.put("Gmail", email);
                //headers.put("accept", "accept");
                return headers;
            }
        };

        int socketTimeout = 50000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        categoryReq.setRetryPolicy(policy);
        mRequestQueue.add(categoryReq);
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(categoryReq);

    }


    private void saveIntoDatabase(ArrayList<Category> categoryArrayList) {

        DAO dao = new DAO(getApplicationContext());
        dao.open();

        for (int i = 0; i < categoryArrayList.size(); i++) {
            boolean found = dao.messageExists(categoryArrayList.get(i).getId());

            if (found) {
                dao.UpdateCategoryItem(categoryArrayList.get(i).getId(),
                        new Category(categoryArrayList.get(i).getCategory(),
                                categoryArrayList.get(i).getCflag()));

                //  Toast.makeText(getApplicationContext(), "Data Updated!", Toast.LENGTH_LONG).show();


            } else {
                DAO.executeSQL("INSERT INTO " + TABLE_CATEGORY + "(id, category, cflag) " +
                                "VALUES(?, ?, ?)",

                        new String[]{categoryArrayList.get(i).getId(), categoryArrayList.get(i).getCategory(),
                                String.valueOf(categoryArrayList.get(i).getCflag())});
                // Toast.makeText(getApplicationContext(), "Data Inserted!", Toast.LENGTH_LONG).show();

            }
        }
    }

}
