package net.qsoft.breakingnews.Service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import net.qsoft.breakingnews.NetworkService.Utils;


public class StartNotificationServiceReceiver extends BroadcastReceiver {

    public StartNotificationServiceReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.N) {
                 Intent service = new Intent(context, NotificationService.class);
                 context.startService(service);
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(new Intent(context, NotificationService.class));
            } else {
                context.startService(new Intent(context, NotificationService.class));
            }

            // Intent service = new Intent(context, NotificationService.class);
            // context.startService(service);
            Utils.log("Start Service: " + "NotificationService");
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }
}
