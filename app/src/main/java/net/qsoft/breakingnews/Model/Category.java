package net.qsoft.breakingnews.Model;

public class Category {
    public static final int FLAG_FOR_CATEGORY = 1;
    private String id;
    private String category;
    private int cflag;


    private boolean ISchecked;

    public Category() {

    }

    public Category(String id, String category, int cflag) {
        this.id = id;
        this.category = category;
        this.cflag=cflag;
    }

    public Category(String category) {
        this.category = category;
    }

    public Category(String category, int cflag) {
        this.category = category;
        this.cflag=cflag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isISchecked() {
        return ISchecked;
    }

    public void setISchecked(boolean ISchecked) {
        this.ISchecked = ISchecked;
    }


    public int getCflag() {
        return cflag;
    }

    public int setCflag(int cflag) {
        this.cflag = cflag;
        return cflag;
    }

}
