package net.qsoft.breakingnews.Model;

import java.util.ArrayList;

/**
 * Created by shah md imtiyaj uddin
 */
public class SectionDataModel {


    private String headerTitle;
    private ArrayList<Message> allItemsInSection;


    public SectionDataModel() {

    }

    public SectionDataModel(String headerTitle, ArrayList<Message> allItemsInSection) {
        this.headerTitle = headerTitle;
        this.allItemsInSection = allItemsInSection;
    }


    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
    }

    public ArrayList<Message> getAllItemsInSection() {
        return allItemsInSection;
    }

    public void setAllItemsInSection(ArrayList<Message> allItemsInSection) {
        this.allItemsInSection = allItemsInSection;
    }

}
