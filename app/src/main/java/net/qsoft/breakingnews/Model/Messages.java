package net.qsoft.breakingnews.Model;

public class Messages {
    public static final int FLAG_FOR_FAVOURITE= 1;

    private String id;
    private String title;
    private String message;
    private String date;
    private String time;
    private int status;
    private int flag;


    public Messages() {

    }

    public Messages(String id, String title, String message, int status,int flag) {
        this.id=id;
        this.title=title;
        this.message=message;
        this.status=status;
        this.flag=flag;
    }

    public Messages(String id, String title, String message, String time) {
        this.id=id;
        this.title=title;
        this.message=message;
        this.time=time;


    }

    public String getTitle() {
        return title;
    }

    public String setTitle(String title) {
        this.title = title;
        return title;

    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }



    public String getId() {
        return id;
    }

    public String setId(String id) {
        this.id = id;
        return id;
    }

    public String getMessage() {
        return message;
    }

    public String setMessage(String message) {
        this.message = message;
        return message;
    }

    public int getStatus() {
        return status;
    }

    public int setStatus(int status) {
        this.status = status;
        return status;
    }


    public int getFlag() {
        return status;
    }

    public int setFlag(int flag) {
        this.flag = flag;
        return flag;
    }
}
