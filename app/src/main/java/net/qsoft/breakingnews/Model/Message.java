package net.qsoft.breakingnews.Model;

public class Message {
    public static final int FLAG_FOR_FAVOURITE = 1;
    public static final int READ = 1;
    public static final int UNREAD = 0;

    private String id;
    private String title;
    private String message;
    private String date;
    private String time;
    private int status;
    private int flag;
    private int favourite;
    private boolean selected;
    private boolean isImportant=true;
    private int FbId;
    private String Gmail;
    private String category;



    public Message() {

    }

    public Message(String id, String title, String message, int FbId, String Gmail, int status, int flag,int favourite) {
        this.id = id;
        this.title = title;
        this.message = message;
        this.FbId=FbId;
        this.Gmail=Gmail;
        this.status = status;
        this.flag = flag;
        this.favourite = favourite;

    }

    public Message(String id, String title, String message,String category, int status, int flag,int favourite) {
        this.id = id;
        this.title = title;
        this.message = message;
        this.status = status;
        this.flag = flag;
        this.favourite = favourite;
        this.category=category;

    }

    public Message(String id, String title, String message, int status, int flag,String time,int favourite) {
        this.id = id;
        this.title = title;
        this.message = message;
        this.status = status;
        this.flag = flag;
        this.time=time;
        this.favourite = favourite;

    }

    public Message(String id, String title, String message, String time) {
        this.id = id;
        this.title = title;
        this.message = message;
        this.time = time;
    }

    // Test recylerview delete all item
    public Message(String id, String title, String message, boolean selected) {
        this.id = id;
        this.title = title;
        this.message = message;
        this.selected = selected;
    }

    public Message(String id, String title, String message, String category, int status, int flag,String time,int favourite) {

        this.id = id;
        this.title = title;
        this.message = message;
        this.status = status;
        this.flag = flag;
        this.favourite = favourite;
        this.category=category;
        this.time=time;
    }


    public String getTitle() {
        return title;
    }

    public String setTitle(String title) {
        this.title = title;
        return title;

    }

    private boolean checked = false;


    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


    public String getId() {
        return id;
    }

    public String setId(String id) {
        this.id = id;
        return id;
    }

    public String getMessage() {
        return message;
    }

    public String setMessage(String message) {
        this.message = message;
        return message;
    }

    public int getStatus() {
        return status;
    }

    public int setStatus(int status) {
        this.status = status;
        return status;
    }


    public int getFlag() {
        return status;
    }

    public int setFlag(int flag) {
        this.flag = flag;
        return flag;
    }

    public int getFavourite() {
        return favourite;
    }

    public int setFavourite(int favourite) {
        this.favourite = favourite;
        return favourite;

    }


    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }


    public boolean isImportant() {
        return isImportant;
    }

    public void setImportant(boolean important) {
        isImportant = important;
    }


    public int getFbId() {
        return FbId;
    }

    public void setFbId(int fbId) {
        FbId = fbId;
    }

    public String getGmail() {
        return Gmail;
    }

    public void setGmail(String gmail) {
        Gmail = gmail;
    }

    public String getCategory() {
        return category;
    }

    public String setCategory(String category) {
        this.category = category;
        return category;
    }

}
