package net.qsoft.breakingnews.Model;

import net.qsoft.breakingnews.Database.DAO;
import net.qsoft.breakingnews.app.AppController;

public class ActiveUser {

    private String FbProfileName;
    private long FbId;
    private String GmailProfileName;
    private String Gmail;
    private String photoUrl;
    private String Ucategory;


    private static ActiveUser mInstance = null;

    private ActiveUser() {
        DAO da = new DAO(AppController.getInstance());
        da.open();
        da.getActiveUser(this);
        da.close();
    }

    public static synchronized ActiveUser getInstance() {
        if (mInstance == null) {
            mInstance = new ActiveUser();
        }
        return mInstance;
    }


    public ActiveUser(String FbProfileName, Long FbId, String photoUrl,String Ucategory) {
         this.FbProfileName = FbProfileName;
         this.FbId = FbId;
         this.photoUrl = photoUrl;
         this.Ucategory=Ucategory;

    }

    public String getFbProfileName() {
        return FbProfileName;
    }

    public void setFbProfileName(String fbProfileName) {
        FbProfileName = fbProfileName;
    }

    public long getFbId() {
        return FbId;
    }

    public void setFbId(long fbId) {
        FbId = fbId;
    }

    public String getGmailProfileName() {
        return GmailProfileName;
    }

    public void setGmailProfileName(String gmailProfileName) {
        GmailProfileName = gmailProfileName;
    }

    public String getGmail() {
        return Gmail;
    }

    public void setGmail(String gmail) {
        Gmail = gmail;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getUcategory() {
        return Ucategory;
    }

    public void setUcategory(String ucategory) {
        Ucategory = ucategory;
    }
}
