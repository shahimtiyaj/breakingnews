package net.qsoft.breakingnews.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.spark.submitbutton.SubmitButton;

import net.qsoft.breakingnews.Adapter.CategorySelectAdapter;
import net.qsoft.breakingnews.Database.DAO;
import net.qsoft.breakingnews.Model.ActiveUser;
import net.qsoft.breakingnews.Model.Category;
import net.qsoft.breakingnews.NetworkService.Utils;
import net.qsoft.breakingnews.NetworkService.VolleyCustomRequest;
import net.qsoft.breakingnews.R;
import net.qsoft.breakingnews.app.AppController;
import net.qsoft.breakingnews.utils.CloudRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.android.volley.VolleyLog.TAG;
import static net.qsoft.breakingnews.Activity.GmailActivity.email;
import static net.qsoft.breakingnews.Activity.GmailActivity.fb_id;
import static net.qsoft.breakingnews.Activity.GmailActivity.fb_profile_name;
import static net.qsoft.breakingnews.Activity.GmailActivity.fbphotoUrl;
import static net.qsoft.breakingnews.Activity.GmailActivity.personName;
import static net.qsoft.breakingnews.Activity.GmailActivity.personPhotoUrl;
import static net.qsoft.breakingnews.Service.NotificationService.MyPREFERENCES;

public class CategoryActivity extends AppCompatActivity {

    ListView lv;
    Context context;
    String[] category_list;
    ArrayList<String> list;
    ArrayList<Category> categoryArrayList;
    private CategorySelectAdapter adapter;
    String req;

    Button categoryBtn;
    Toolbar toolbar;
    private static String key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setTitle("Category List");
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        context = this;

        // list = new ArrayList<String>();

        DAO dao = new DAO(AppController.getInstance());
        dao.open();
        categoryArrayList = dao.getAllCategory();

        SubmitButton sb = (SubmitButton) findViewById(R.id.categoryBtn);

        sb.setOnClickListener(new View.OnClickListener() {
            static final String TAG = "onclick";

            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(getApplicationContext())) {
                    SendCategoryToServer();
                } else {
                    noInternetConnection();
                }

                Log.d(TAG, "onClick: Submit");
            }
        });

//        categoryBtn=(Button)findViewById(R.id.categoryBtn);
//
//        categoryBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ServerData();
//            }
//        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // category_list = getResources().getStringArray(R.array.category_list);

        lv = (ListView) findViewById(R.id.category_listview);
        lv.setAdapter(new CategorySelectAdapter(this, categoryArrayList));
        lv.setDivider(null);

       // GetCategory();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goToHome();
    }

    public void noInternetConnection() {
        //network not available custom toast---
        Context context = getApplicationContext();
        LayoutInflater inflater = getLayoutInflater();
        View customToastroot = inflater.inflate(R.layout.network_no_connection, null);
        Toast customtoast = new Toast(context);
        customtoast.setView(customToastroot);
        customtoast.setGravity(Gravity.BOTTOM | Gravity.CENTER, 0, 0);
        customtoast.setDuration(Toast.LENGTH_LONG);
        customtoast.show();
    }

    public void goToHome() {
        SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences(
                MyPREFERENCES, 0);

        String key1 = sharedpreferences.getString("flag", key);

        if (key1.equals("1")) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("personPhotoUrl", personPhotoUrl);
            intent.putExtra("personName", personName);
            intent.putExtra("email", email);
            intent.putExtra("key", 1);

            startActivity(intent);
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
            finish();
        } else {

            Intent goToFbUserActivity = new Intent(this, MainActivity.class);
            //goToFbUserActivity.putExtra("EMAIL", email);
            goToFbUserActivity.putExtra("NAME", fb_profile_name);
            goToFbUserActivity.putExtra("ID", fb_id);
            // goToFbUserActivity.putExtra("GENDER", gender);
            goToFbUserActivity.putExtra("PHOTO_URL", fbphotoUrl);
            goToFbUserActivity.putExtra("key", 5);

            startActivity(goToFbUserActivity);
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void SendCategoryToServer() {


        //String hitUrl = "http://192.168.0.117/JeneRakhun/public/userinfocategory";
        //String hitUrl = "http://qsoft.net/khobor/public/userinfocategory";
       // String hitUrl = "http://192.168.0.17:8080/khobor/public/userinfocategory";

        String hitUrl = AppController.getSendCategoryToserverUrl();


        CategoryGetDataSqlite();
        HashMap<String, String> params = new HashMap<>();
        params.put("req", req);


        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());

        VolleyCustomRequest postRequest = new VolleyCustomRequest(Request.Method.POST, hitUrl, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "Server Response: " + response.toString());

                        try {
                            Log.v("Response:%n %s", response.toString(4));
                            String status = response.getString("status");
                            Log.d("Message", status);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.d(TAG, "Error: " + volleyError.getMessage());

                        if (volleyError instanceof NetworkError) {
                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();
                        } else if (volleyError instanceof ServerError) {
                            Toast.makeText(getApplicationContext(), "The server could not be found. Please try again after some time!!", Toast.LENGTH_SHORT).show();

                        } else if (volleyError instanceof AuthFailureError) {
                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();


                        }

//                        else if (volleyError instanceof ParseError) {
//                            Toast.makeText(getApplicationContext(), "Parsing error! Please try again after some time!!", Toast.LENGTH_SHORT).show();
//                        }

                        else if (volleyError instanceof NoConnectionError) {
                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();

                        } else if (volleyError instanceof TimeoutError) {
                            Toast.makeText(getApplicationContext(), "Connection TimeOut! Please check your internet connection", Toast.LENGTH_SHORT).show();

                        }

                        try {
                            Log.e("myerror", "deal first", volleyError);
                            Log.e("myerror", "deal success" + new String(volleyError.networkResponse.data, "utf-8"));
                        } catch (Throwable e) {
                            Log.e("myerror", "deal fail", e);
                        }
                    }
                })

//        {
//            /**
//             * Passing some request headers
//             */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//
//                headers.put("FbProfileName", fb_profile_name);
//                headers.put("FbId", String.valueOf(fb_id));
//                headers.put("GmailProfileName", personName);
//                headers.put("Gmail", email);
//
//                return headers;
//            }
//
//        }

                ;

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        mRequestQueue.add(postRequest);

        // Volley.newRequestQueue(this).add(postRequest);

        CloudRequest.getInstance(this).addToRequestQueue(postRequest);
    }


    private void CategoryGetDataSqlite() {
        ActiveUser usr = ActiveUser.getInstance();
        String fbid = String.valueOf(usr.getFbId());
        String gmail = usr.getGmail();

        categoryArrayList = new ArrayList<Category>();
        try {

            // List<GoodLoans> goodLoans = databaseHandler.getAllGoodLoanList();

            DAO dao = new DAO(getApplicationContext());
            dao.open();
            //    ArrayList<Category> categories = dao.getAllCategory();
            categoryArrayList = dao.getAllSelectedCategory();

            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObj = new JSONObject();

            for (Category category : categoryArrayList) {
                JSONObject jsonObject = new JSONObject();

                String CategoryId = category.getId();
                String alertCategory = category.getCategory();
                String categoryFlag= String.valueOf(category.getCflag());

                jsonObject.put("id", CategoryId);
                jsonObject.put("categoryType", alertCategory);
                jsonObject.put("categoryFlag", categoryFlag);


                jsonArray.put(jsonObject);
                //  jsonObject.put("req=",jsonObj);

            }

            JSONObject obj = new JSONObject();

            try {
                obj.put("status", "sucess");
                obj.put("fbid", fbid);
                obj.put("gmail", email);


                obj.put("Data", jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            req = obj.toString();

            Utils.log("Category user: "+req);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}


