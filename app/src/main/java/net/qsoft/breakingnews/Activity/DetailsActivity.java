package net.qsoft.breakingnews.Activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.hitomi.cmlibrary.CircleMenu;
import com.hitomi.cmlibrary.OnMenuSelectedListener;
import com.hitomi.cmlibrary.OnMenuStatusChangeListener;

import net.qsoft.breakingnews.app.AppController;
import net.qsoft.breakingnews.Database.DAO;
import net.qsoft.breakingnews.Model.Message;
import net.qsoft.breakingnews.R;
import net.qsoft.breakingnews.utils.ClipBoardManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static net.qsoft.breakingnews.Activity.GmailActivity.email;
import static net.qsoft.breakingnews.Activity.GmailActivity.fb_id;
import static net.qsoft.breakingnews.Activity.GmailActivity.fb_profile_name;
import static net.qsoft.breakingnews.Activity.GmailActivity.fbphotoUrl;
import static net.qsoft.breakingnews.Activity.GmailActivity.personName;
import static net.qsoft.breakingnews.Activity.GmailActivity.personPhotoUrl;
import static net.qsoft.breakingnews.Database.DBHelper.TABLE_ARCHIVE;
import static net.qsoft.breakingnews.Service.NotificationService.MyPREFERENCES;
import static net.qsoft.breakingnews.app.AppController.defaultImageUrl;

public class DetailsActivity extends AppCompatActivity {
    TextView user_id, message_title, message_body, message_date, message_time, date_time;
    private String id, title, message,category, date, time, weekName, Today, time1, time2, time3, twodays, yesterday;
    private int status;
    private Button copyBtn;
    public ArrayList<Message> messagesList;
    private String local_data_id;
    private CircleMenu circleMenu;
    Dialog myDialog;
    Toolbar toolbar;
    View v;
    private static String key;
    ScaleGestureDetector scaleGestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // requestFullScreenWindow();
        setContentView(R.layout.activity_details);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        // toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        myDialog = new Dialog(this);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

      //  copyBtn = (Button) findViewById(R.id.btn_copy);

//        ToggleButton toggle = (ToggleButton) findViewById(R.id.toggle);
//
//        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    Toast.makeText(getApplicationContext(),"ON",Toast.LENGTH_LONG).show();
//                } else {
//                    Toast.makeText(getApplicationContext(),"OFF",Toast.LENGTH_LONG).show();
//                }
//            }
//        });


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // circleMenu = (CircleMenu) findViewById(R.id.circle_menu);
        // CircleMenuView();

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                id = null;
                title = null;
                message = null;
                category = null;

                date = null;
                time = null;
                weekName = null;
                Today = null;
                yesterday = null;
                twodays = null;
                status = Integer.parseInt(null);
                getSupportActionBar().setTitle("Message");

            } else {
                id = extras.getString("id");
                title = extras.getString("title");
                message = extras.getString("message");
                category = extras.getString("category");

                date = extras.getString("date");
                time = extras.getString("time");
                weekName = extras.getString("weekName");
                Today = extras.getString("today");
                yesterday = extras.getString("yesterday");
                twodays = extras.getString("twodaysAgo");

                status = extras.getInt("status");
                status = 1;

                SimpleDateFormat format = new SimpleDateFormat("EEEE, d'th' MMMM yyyy", Locale.US);
                String cdate = format.format(new Date());

                if (Today.equals(weekName)) {
                    time1 = "Today:" + time;
                    setView(title, message, date, time1);
                } else if (weekName.equals(yesterday)) {
                    time2 = "Yesterday:" + time;
                    setView(title, message, date, time2);
                } else if (weekName.equals(twodays)) {
                    time3 = "2 days ago: " + time;
                    setView(title, message, date, time3);
                } else {
                    setView(title, message, date, time);
                }

                // setView(title, message,date,time);
                getSupportActionBar().setTitle(category);

                // DAO.executeSQL("UPDATE" +TABLE_NOTIFICATION+ "SET status = \"1\" WHERE id =" +id);

                DAO dao = new DAO(AppController.getInstance());
                dao.open();
                dao.UpdateStatus(status, id);
                dao.close();

//                // Link to log in Screen
//                copyBtn.setOnClickListener(new View.OnClickListener() {
//
//                    public void onClick(View view) {
//                        SendSms();
//                    }
//                });
            }
        } else {
              category = (String) savedInstanceState.getSerializable("category");
            title = (String) savedInstanceState.getSerializable("title");
            message = (String) savedInstanceState.getSerializable("message");
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setView(final String title, final String message, final String date, final String time) {
        final LinearLayout mealLayout = (LinearLayout) findViewById(R.id.message_body_layout);
        final CardView cardViewLayout = (CardView) findViewById(R.id.card_view1);


        user_id = (TextView) findViewById(R.id.id);
        message_title = (TextView) findViewById(R.id.title);
        message_body = (TextView) findViewById(R.id.message);
        message_date = (TextView) findViewById(R.id.date_notify);
        message_time = (TextView) findViewById(R.id.time_notify);
      //  message_body.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);


        // user_id.setText(id);
        message_title.setText(title);
        message_body.setText(message);
        message_date.setText(date);
        message_time.setText(time);
        message_body.setTextIsSelectable(true);

        scaleGestureDetector = new ScaleGestureDetector(this,new simpleOnScaleGestureListener());
        message_body.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                event.setLocation(event.getRawX(), event.getRawY());
                scaleGestureDetector.onTouchEvent(event);

                return true;
            }
        });


        mealLayout.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                mealLayout.setBackgroundColor(getResources().getColor(R.color.row_activated));
                AlertAfterDetailsLongPress();
                return true;
            }
        });
    }


    public class simpleOnScaleGestureListener extends
            ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {

            // TODO Auto-generated method stub
            float size = message_body.getTextSize();
            Log.d("TextSizeStart", String.valueOf(size));

            float factor = detector.getScaleFactor();
            Log.d("Factor", String.valueOf(factor));


            float product = size*factor;
            Log.d("TextSize", String.valueOf(product));
            message_body.setTextSize(TypedValue.COMPLEX_UNIT_PX, product);

            size = message_body.getTextSize();
            Log.d("TextSizeEnd", String.valueOf(size));

            return true;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub
        scaleGestureDetector.onTouchEvent(event);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_notifications, menu);

        return true;
    }

    @Override
    public boolean onMenuOpened(int featureId, Menu menu) {
        circleMenu.openMenu();


        return super.onMenuOpened(featureId, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int option_id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (option_id == R.id.btn_delete) {
            Alert();
            return true;
        }
        if (option_id == R.id.btn_copy) {
            Copy();
            return true;
        }

        if (option_id == R.id.btn_sendSms) {
            SendSms();
            return true;
        }
        if (option_id == R.id.btn_share) {
            Share();
            return true;
        }

    /*    if (option_id == R.id.btn_like) {

            item.setIcon(R.drawable.ic_favorite_black_24dp);

            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());


            DAO.executeSQL("INSERT OR REPLACE INTO " + TABLE_ARCHIVE + "(id, title, message, time) " +
                            "VALUES(?, ?, ?, ?)",

                    new String[]{id, title, message, currentDateTimeString});


            Toast.makeText(getApplicationContext(), "Added to favourite list !", Toast.LENGTH_LONG).show();

            return true;
        } */

        return super.onOptionsItemSelected(item);
    }

    private void requestFullScreenWindow() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        circleMenu.closeMenu();
        goToHome();
    }

    public void goToHome() {
        SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences(
                MyPREFERENCES, 0);

        String key1 = sharedpreferences.getString("flag", key);

        if (key1.equals("1")) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            if (personPhotoUrl == null) {
                intent.putExtra("personPhotoUrl", defaultImageUrl);
            } else {
                intent.putExtra("personPhotoUrl", personPhotoUrl);
            }
            intent.putExtra("personName", personName);
            intent.putExtra("email", email);
            intent.putExtra("key", 1);
            startActivity(intent);
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
            finish();
        } else {

            Intent goToFbUserActivity = new Intent(this, MainActivity.class);
            //goToFbUserActivity.putExtra("EMAIL", email);
            goToFbUserActivity.putExtra("NAME", fb_profile_name);
            goToFbUserActivity.putExtra("ID", fb_id);
            // goToFbUserActivity.putExtra("GENDER", gender);
            goToFbUserActivity.putExtra("PHOTO_URL", fbphotoUrl);
            goToFbUserActivity.putExtra("key", 5);

            startActivity(goToFbUserActivity);
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
            finish();
        }
    }

    void SimpleDateAndTime() {

        Calendar calendar;
        calendar = Calendar.getInstance();
        //date format is:  "Date-Month-Year Hour:Minutes am/pm"
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm a"); //Date and time
        String currentDate = sdf.format(calendar.getTime());

        //Day of Name in full form like,"Saturday", or if you need the first three characters you have to put "EEE" in the date format and your result will be "Sat".
        SimpleDateFormat sdf_ = new SimpleDateFormat("EEEE");
        Date date = new Date();
        String dayName = sdf_.format(date);
        date_time.setText("" + dayName + " " + currentDate + "");

    }

//    public void ShowPopup(View v) {
//
//        Button ok, cencel;
//
//        myDialog.setContentView(R.layout.custom_pop_up);
//        ok = (Button) myDialog.findViewById(R.id.btn_ok);
//        cencel = (Button) myDialog.findViewById(R.id.btn_cencel);
//
//        ok.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                DAO dao = new DAO(AppController.getInstance());
//                dao.open();
//                dao.deleteSingleMessage(id);
//                dao.close();
//                Toast.makeText(getApplicationContext(), "Deleted!", Toast.LENGTH_LONG).show();
//            }
//        });
//
//        cencel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                myDialog.dismiss();
//            }
//        });
//        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        myDialog.show();
//    }


    public void Alert() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Attention Please !")
                .setMessage("Are you sure you want to delete this message!")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DAO dao = new DAO(AppController.getInstance());
                        dao.open();
                        dao.deleteSingleMessage(id);
                        dao.close();
                        Toast.makeText(getApplicationContext(), "1 Message Deleted!", Toast.LENGTH_LONG).show();
                        goToHome();

                    }

                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                }).show();
    }

    public void AlertAfterDetailsLongPress() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Message options");

        String[] animals = {"Delete", "Copy", "Share", "Feedback", "Rate us", "View message details"};
        builder.setItems(animals, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: // delete
                        Alert();
                        break;
                    case 1: // copy text
                        Copy();
                        break;
                    case 2: // share
                        Share();
                        break;
                    case 3: // feedback
                        Feedback();
                        break;
                    case 4: // rate us

                        Rate();
                    case 5: // view details
                        DetailsView();

                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void DetailsView() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Message details");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();;
            }
        });

        String[] animals = {"Category type :--", category, "From :--", "Alert Service App", "Received :--", date};
        builder.setItems(animals, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void Copy() {
        ClipBoardManager clipBoardManager = new ClipBoardManager();
        clipBoardManager.copyToClipboard(getApplicationContext(), title + ":" + "\n" + message);
        Toast.makeText(getApplicationContext(), "Copied text", Toast.LENGTH_LONG).show();
    }

    public void Share() {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, title);
        //String sAux = "\nBreaking news \n\n";
        // sAux = sAux + "https://play.google.com/store/apps/details?id=net.qsoft.breakingnews\n\n";
        i.putExtra(Intent.EXTRA_TEXT, message);
        startActivity(Intent.createChooser(i, "Select One"));
        // Toast.makeText(getApplicationContext(), "3", Toast.LENGTH_LONG).show();
    }

    public void Feedback() {
        Intent email = new Intent(Intent.ACTION_SEND);
        email.setType("text/email");
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{"shahimtiyaj94@gmail.com"});
        email.putExtra(Intent.EXTRA_SUBJECT, "Feedback about Braking new App!");
        email.putExtra(Intent.EXTRA_TEXT, "Hello, " + "\n");
        startActivity(Intent.createChooser(email, "Send Feedback:"));

    }

    public void Rate() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("market://details?id=net.qsoft.breakingnews"));
        startActivity(intent);
    }

    public void SendSms() {

        Intent I = new Intent(Intent.ACTION_VIEW);

        I.setData(Uri.parse("smsto:"));
        I.setType("vnd.android-dir/mms-sms");
        I.putExtra("address", new String(""));
        I.putExtra("sms_body", title + ":" + "\n" + message);

        try {
            startActivity(I);
            finish();
            Log.i("Sms Send", "");
        } catch (Exception e) {
            Toast.makeText(DetailsActivity.this, "Sms not send", Toast.LENGTH_LONG).show();
        }

    }

    public void CircleMenuView() {
        circleMenu.setMainMenu(Color.parseColor("#08b2e1"), R.drawable.view, R.drawable.ic_close_black_24dp)
                .addSubMenu(Color.parseColor("#258CFF"), R.drawable.ic_home_white)
                .addSubMenu(Color.parseColor("#30A400"), R.drawable.ic_send_black_24dp)
                .addSubMenu(Color.parseColor("#FF4B32"), R.drawable.ic_content_copy_black_24dp)
                .addSubMenu(Color.parseColor("#8A39FF"), R.drawable.ic_share_black_24dp)
                .addSubMenu(Color.parseColor("#FF6A00"), R.drawable.ic_like)
                .setOnMenuSelectedListener(new OnMenuSelectedListener() {

                    @Override
                    public void onMenuSelected(int index) {

                        switch (index) {
                            case 0:
                                //ShowPopup(v);
                                goToHome();
                                break;

                            case 1:

                                Intent I = new Intent(Intent.ACTION_VIEW);

                                I.setData(Uri.parse("smsto:"));
                                I.setType("vnd.android-dir/mms-sms");
                                I.putExtra("address", new String(""));
                                I.putExtra("sms_body", title + ":" + "\n" + message);

                                try {
                                    startActivity(I);
                                    finish();
                                    Log.i("Sms Send", "");
                                } catch (Exception e) {
                                    Toast.makeText(DetailsActivity.this, "Sms not send", Toast.LENGTH_LONG).show();
                                }

                                break;

                            case 2:

                                ClipBoardManager clipBoardManager = new ClipBoardManager();
                                clipBoardManager.copyToClipboard(getApplicationContext(), title + ":" + "\n" + message);
                                // Snackbar.make(v,"Copied To Clipboard",Snackbar.LENGTH_SHORT).show();

                                Toast.makeText(getApplicationContext(), "Copied text", Toast.LENGTH_LONG).show();
                                break;
                            case 3:

                                Intent i = new Intent(Intent.ACTION_SEND);
                                i.setType("text/plain");
                                i.putExtra(Intent.EXTRA_SUBJECT, title);
                                //String sAux = "\nBreaking news \n\n";
                                // sAux = sAux + "https://play.google.com/store/apps/details?id=net.qsoft.breakingnews\n\n";
                                i.putExtra(Intent.EXTRA_TEXT, message);
                                startActivity(Intent.createChooser(i, "Select One"));
                                // Toast.makeText(getApplicationContext(), "3", Toast.LENGTH_LONG).show();

                                break;
                            case 4:

                                Intent email = new Intent(Intent.ACTION_SEND);
                                email.setType("text/email");
                                email.putExtra(Intent.EXTRA_EMAIL, new String[]{"shahimtiyaj94@gmail.com"});
                                email.putExtra(Intent.EXTRA_SUBJECT, "Feedback about Braking new App!");
                                email.putExtra(Intent.EXTRA_TEXT, "Hello, " + "\n");
                                startActivity(Intent.createChooser(email, "Send Feedback:"));
                                //Toast.makeText(getApplicationContext(), "4", Toast.LENGTH_LONG).show();
                                break;

                            default:
                                break;
                        }

                    }

                }).setOnMenuStatusChangeListener(new OnMenuStatusChangeListener() {

            @Override
            public void onMenuOpened() {

            }

            @Override
            public void onMenuClosed() {

            }

        });
    }
}
