package net.qsoft.breakingnews.Activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import net.qsoft.breakingnews.Fragment.FragmentAboutUs;
import net.qsoft.breakingnews.Fragment.FragmentFavouriteList;
import net.qsoft.breakingnews.Fragment.FragmentReadMessageList;
import net.qsoft.breakingnews.Fragment.FragmentUnreadMsgList;
import net.qsoft.breakingnews.Fragment.TabFragment;
import net.qsoft.breakingnews.NetworkService.Utils;
import net.qsoft.breakingnews.NetworkService.VolleyCustomRequest;
import net.qsoft.breakingnews.R;
import net.qsoft.breakingnews.Service.NotificationSchedulerService;
import net.qsoft.breakingnews.Session.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static net.qsoft.breakingnews.Activity.GmailActivity.email;
import static net.qsoft.breakingnews.Activity.GmailActivity.fb_id;
import static net.qsoft.breakingnews.Service.NotificationService.MyPREFERENCES;
import static net.qsoft.breakingnews.Service.NotificationService.sum;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener {

    TextView smsCountTxt;
    int pendingSMSCount = 0;

    private ShareDialog shareDialog;
    private Button logout;

    private static final String TAG = "GoogleSignIn";
    private static final int RC_SIGN_IN = 9001;

    private final Context mContext = this;

    private GoogleApiClient mGoogleApiClient;
    private TextView mStatusTextView, mUserTextView, mEmailTextView;
    private CircleImageView mProfileImageView;
    private ProgressDialog mProgressDialog;

    protected String key;

    String pImage, uName, uemail;
    String facebookImageUrl, facebookProfileName, facebookEmail;

    //category -----------------------------
    Button btnCategory;
    TextView mItemSelected;
    String[] listItems;
    boolean[] checkedItems;
    ArrayList<Integer> mUserItems = new ArrayList<>();
    Dialog myDialog;
    private View view;
    String category;

    // Session Manager Class
    SessionManager session;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        myDialog = new Dialog(this);

      // showDebugDBAddressLogToast(getApplicationContext());

        startService(new Intent(MainActivity.this, NotificationSchedulerService.class));
        Utils.log("Start Service: " + "NotificationSchedulerService");

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        TabFragment tabFragment = new TabFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, tabFragment);
        fragmentTransaction.commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        // Views gmail profile init-------------------------------------
        mProfileImageView = (CircleImageView) navigationView.getHeaderView(0).findViewById(R.id.profileImage);
        mUserTextView = (TextView) navigationView.getHeaderView(0).findViewById(R.id.userName);
        mEmailTextView = (TextView) navigationView.getHeaderView(0).findViewById(R.id.email);

        //Facebook login info--------------------------------------------
        shareDialog = new ShareDialog(this);


//        Button logout = (Button) findViewById(R.id.logout);
//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                LoginManager.getInstance().logOut();
//                Intent login = new Intent(MainActivity.this, LoginActivity2.class);
//                startActivity(login);
//                finish();
//            }
//        });


        //Gmail login and details show in Nav drawer --------------------------------------------------------

        Bundle inBundle = getIntent().getExtras();
        key = inBundle.get("key").toString();

        SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("flag", key);
        editor.apply();

        if (key.equals("1")) {
//            if (pImage == null) {
//                uName = inBundle.get("personName").toString();
//                uemail = inBundle.get("email").toString();
//            } else {
            pImage = inBundle.get("personPhotoUrl").toString();
            uName = inBundle.get("personName").toString();
            uemail = inBundle.get("email").toString();
            // }

            // Views gmail profile set-------------------------
            mUserTextView.setText(uName);
            mEmailTextView.setText(uemail);

            Glide.with(getApplicationContext()).load(pImage)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(mProfileImageView);

            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this, this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        } else {

            //String key1 = getIntent().getStringExtra("key");

            if (key.equals("5")) {
//           Bundle innBundle = getIntent().getExtras();
//           String name = innBundle.get("NAME").toString();
//           String imageUrl = innBundle.get("PHOTO_URL").toString();

                //get intent which has our user data

                facebookProfileName = getIntent().getStringExtra("NAME");
                // facebookEmail = getIntent().getStringExtra("EMAIL");
                // String gender = getIntent().getStringExtra("GENDER");
                facebookImageUrl = getIntent().getStringExtra("PHOTO_URL");
                //   Log.d("Imtiyaj: ", facebookImageUrl);

                //facebook login details
                mProfileImageView = (CircleImageView) navigationView.getHeaderView(0).findViewById(R.id.profileImage);
                mUserTextView = (TextView) navigationView.getHeaderView(0).findViewById(R.id.userName);
                mUserTextView.setText(facebookProfileName);

                Glide.with(getApplicationContext()).load(facebookImageUrl)
                        .thumbnail(0.5f)
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(mProfileImageView);


//                DAO.executeSQL("INSERT OR REPLACE INTO " + TABLE_USERINFO +
//                        "(FbProfileName, FbId, GmailProfileName, Gmail, fbphotoUrl) " +
//                        "VALUES(?, ?, ?, ?, ?)", new String[]{facebookProfileName, "0", "None", "None", facebookImageUrl});
//


            }

            // Add code to print out the key hash----------------------------------
            try {
                @SuppressLint("PackageManagerGetSignatures") PackageInfo info = getPackageManager().getPackageInfo(
                        "net.qsoft.breakingnews",  //Replace your package name here
                        PackageManager.GET_SIGNATURES);
                for (Signature signature : info.signatures) {
                    MessageDigest md = MessageDigest.getInstance("SHA");
                    md.update(signature.toByteArray());
                    Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                }
            } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException ignored) {

            }

        }
    }


    // [START signOut] gmail
    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        Intent intent = new Intent(MainActivity.this, GmailActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_in, R.anim.right_out);
                        finish();
                    }
                });
    }

    //log out facebook
    private void logOut() {

        if (AccessToken.getCurrentAccessToken() != null) {
            LoginManager.getInstance().logOut();
            Intent intent = new Intent(MainActivity.this, GmailActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
            finish();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(getApplicationContext(), "Connection failed!", Toast.LENGTH_LONG).show();
    }

//
//    @Override
//    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else if (!drawer.isDrawerOpen(GravityCompat.START)) {
//            //super.onBackPressed();
//            //showExit();
//        }
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_notifications);

        View actionView = MenuItemCompat.getActionView(menuItem);
        smsCountTxt = (TextView) actionView.findViewById(R.id.notification_badge);

        setupBadge();

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_notifications) {
            sum = 1;
            Intent intent = new Intent(getApplicationContext(), MessageActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.left_in, R.anim.left_out);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            TabFragment aboutsFragment = new TabFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, aboutsFragment);
            fragmentTransaction.commit();
        } else if (id == R.id.nav_notification) {

            Intent intent = new Intent(getApplicationContext(), MessageActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.left_in, R.anim.left_out);
            finish();


        } else if (id == R.id.nav_read) {
            FragmentReadMessageList readMessageList = new FragmentReadMessageList();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, readMessageList);
            fragmentTransaction.commit();
        } else if (id == R.id.nav_unread) {

            FragmentUnreadMsgList fragmentUnreadMsgList = new FragmentUnreadMsgList();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragmentUnreadMsgList);
            fragmentTransaction.commit();
        } else if (id == R.id.nav_favourite) {
            FragmentFavouriteList aboutsFragment = new FragmentFavouriteList();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, aboutsFragment);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_catagory) {
//            Intent intent = new Intent(getApplicationContext(), FacebookActivity.class);
//            startActivity(intent);
//            overridePendingTransition(R.anim.left_in, R.anim.left_out);
//            finish();

            //       categorySelection();

            Intent intent = new Intent(getApplicationContext(), CategoryActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.left_in, R.anim.left_out);
            finish();

        } else if (id == R.id.nav_share) {

            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, " Breaking new App");
            String sAux = "\nBreaking news \n\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id=net.qsoft.breakingnews\n\n";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "Select One"));

        } else if (id == R.id.nav_rate) {

            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=net.qsoft.breakingnews"));
            startActivity(intent);

        } else if (id == R.id.nav_feedback) {
            Intent email = new Intent(Intent.ACTION_SEND);
            email.setType("text/email");
            email.putExtra(Intent.EXTRA_EMAIL, new String[]{"shahimtiyaj94@gmail.com"});
            email.putExtra(Intent.EXTRA_SUBJECT, "Feedback about Braking new App!");
            email.putExtra(Intent.EXTRA_TEXT, "Hello, " + "\n");
            startActivity(Intent.createChooser(email, "Send Feedback:"));

        } else if (id == R.id.nav_about) {
            FragmentAboutUs aboutsFragment = new FragmentAboutUs();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            overridePendingTransition(R.anim.left_in, R.anim.left_out);
            fragmentTransaction.replace(R.id.fragment_container, aboutsFragment);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_sign_out) {
            if (key.equals("1")) {
                //gmail log out
                signOut();
            } else if (key.equals("5")) {
                //facebook log out
                logOut();
            }

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void setupBadge() {

        if (smsCountTxt != null) {
            if (sum == 1) {
                if (smsCountTxt.getVisibility() != View.GONE) {
                    smsCountTxt.setVisibility(View.GONE);
                }
            } else {
                smsCountTxt.setText(String.valueOf(Math.min(sum, 99)));
                if (smsCountTxt.getVisibility() != View.VISIBLE) {
                    smsCountTxt.setVisibility(View.VISIBLE);
                }
            }
        }
    }


    public void categorySelection() {
        listItems = getResources().getStringArray(R.array.category_list);
        checkedItems = new boolean[listItems.length];

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
        mBuilder.setTitle(R.string.dialog_title);
        mBuilder.setMultiChoiceItems(listItems, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int position, boolean isChecked) {
                if (isChecked) {

                    String selected = listItems[position];

                    SharedPreferences.Editor editor = getSharedPreferences("MyPref", MODE_PRIVATE).edit();
                    editor.putBoolean(selected, true);
                    editor.apply();

                    Toast.makeText(MainActivity.this,
                            "You have selected: " + selected,
                            Toast.LENGTH_LONG).show();

                    mUserItems.add(position);


                } else {
                    mUserItems.remove((Integer.valueOf(position)));

                    Toast.makeText(MainActivity.this,
                            "Deselected!", Toast.LENGTH_LONG).show();
                }
            }
        });

        mBuilder.setCancelable(false);
        mBuilder.setPositiveButton(R.string.ok_label, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                category = "";

                for (int i = 0; i < mUserItems.size(); i++) {
                    category = category + listItems[mUserItems.get(i)];
                    if (i != mUserItems.size() - 1) {
                        category = category + ",";

                        // checkedItems[i] = true;
                    }
                }

                ShowPopup(view);
                //showExit();
                //mItemSelected.setText(category);

            }
        });

        mBuilder.setNegativeButton(R.string.dismiss_label, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        mBuilder.setNeutralButton(R.string.clear_all_label, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                if (mItemSelected != null) {
                    for (int i = 0; i < checkedItems.length; i++) {
                        checkedItems[i] = false;
                        mUserItems.clear();
                        mItemSelected.setText("");
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "No Selected Item!", Toast.LENGTH_LONG).show();
                }
            }
        });

        AlertDialog mDialog = mBuilder.create();
        mDialog.show();
    }

    public void ShowPopup(View view) {

        Button ok, back;
        TextView txtclose, back1;

        myDialog.setContentView(R.layout.custom_pop_up);
        ok = (Button) myDialog.findViewById(R.id.ok);
        back = (Button) myDialog.findViewById(R.id.back);
        mItemSelected = (TextView) myDialog.findViewById(R.id.tvItemSelected1);
        mItemSelected.setText(category);


        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SendingCategory();
                Toast.makeText(getApplicationContext(), "Sending Data to server..", Toast.LENGTH_LONG).show();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                categorySelection();
            }
        });

//        back = (TextView) myDialog.findViewById(R.id.back);
//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                categorySelection();            }
//        });

        txtclose = (TextView) myDialog.findViewById(R.id.txtclose);
        txtclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (drawer.isDrawerOpen(GravityCompat.START) == false) {
            SharedPreferences sharedPreferences = getSharedPreferences("back", MODE_PRIVATE);

            switch (sharedPreferences.getString("now", "")) {

                case "dev":
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new TabFragment()).commit();
                    break;

                case "home":
                    int check = sharedPreferences.getInt("rate", 0);

                    if (check == 0) {
                        showRate();
                    } else {
                        showExit();
                    }
                    break;
            }

        }
    }

    public void showExit() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Are you sure you want to exit News Alert Service?")
                .setTitle("Exit")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        builder.show();
    }


    public void showRate() {
        SharedPreferences sharedPreferences = getSharedPreferences("back", MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        final AlertDialog.Builder rateappBuilder = new AlertDialog.Builder(MainActivity.this);
        rateappBuilder.setMessage("Please rate News Alert Service App on Google Play Store to give us your valuable feedback!")
                .setTitle("Rate News Alert Service ")
                .setPositiveButton("REVIEW", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("market://details?id=net.qsoft.breakingnews"));
                        startActivity(intent);
                        editor.putInt("rate", 1);
                        editor.apply();
                    }
                })
                .setNegativeButton("NEVER", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        editor.putInt("rate", 1);
                        editor.apply();
                        finish();
                    }
                })
                .setNeutralButton("LATER", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

        rateappBuilder.show();
    }


    public void SendingCategory() {

        // String hitURL = "http://192.168.0.117/JeneRakhun/public/userupdatecategory";
       // String hitURL = "http://qsoft.net/khobor/public/userupdatecategory";
        String hitURL = "http://192.168.0.17:8080/khobor/public/userupdatecategory";

        HashMap<String, String> params = new HashMap<>();
        params.put("FbId", String.valueOf(fb_id));
        params.put("Gmail", email);
        params.put("Category", category);

        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());

        VolleyCustomRequest postRequest = new VolleyCustomRequest(Request.Method.POST, hitURL, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "Server Response: " + response.toString());

                        try {
                            Log.v("Response:%n %s", response.toString(4));
                            String status = response.getString("status");
                            Log.d("Message", status);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.d(TAG, "Error: " + volleyError.getMessage());

                        if (volleyError instanceof NetworkError) {
                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();
                        } else if (volleyError instanceof ServerError) {
                            Toast.makeText(getApplicationContext(), "The server could not be found. Please try again after some time!!", Toast.LENGTH_SHORT).show();

                        } else if (volleyError instanceof AuthFailureError) {
                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();


                        } else if (volleyError instanceof ParseError) {
                            Toast.makeText(getApplicationContext(), "Parsing error! Please try again after some time!!", Toast.LENGTH_SHORT).show();

                        } else if (volleyError instanceof NoConnectionError) {
                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();

                        } else if (volleyError instanceof TimeoutError) {
                            Toast.makeText(getApplicationContext(), "Connection TimeOut! Please check your internet connection", Toast.LENGTH_SHORT).show();

                        }

                        try {
                            Log.e("myerror", "deal first", volleyError);
                            Log.e("myerror", "deal success" + new String(volleyError.networkResponse.data, "utf-8"));
                        } catch (Throwable e) {
                            Log.e("myerror", "deal fail", e);
                        }
                    }
                })

//        {
//            /**
//             * Passing some request headers
//             */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//
//                headers.put("FbId", String.valueOf(fb_id));
//                headers.put("Gmail", email);
//                headers.put("Category", category);
//
//                return headers;
//            }
//
//        }
                ;


        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        mRequestQueue.add(postRequest);
        Volley.newRequestQueue(this).add(postRequest);

    }


    @Override
    protected void onResume() {
        super.onResume();

    }

}
