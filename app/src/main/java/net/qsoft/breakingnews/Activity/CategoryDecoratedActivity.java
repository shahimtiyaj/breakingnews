package net.qsoft.breakingnews.Activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import net.qsoft.breakingnews.Adapter.MessageListAdapter;
import net.qsoft.breakingnews.Database.DAO;
import net.qsoft.breakingnews.Model.ActiveUser;
import net.qsoft.breakingnews.Model.Category;
import net.qsoft.breakingnews.Model.Message;
import net.qsoft.breakingnews.R;
import net.qsoft.breakingnews.app.AppController;
import net.qsoft.breakingnews.helper.DividerItemDecoration;

import java.util.ArrayList;

import static net.qsoft.breakingnews.Activity.GmailActivity.email;
import static net.qsoft.breakingnews.Activity.GmailActivity.fb_id;
import static net.qsoft.breakingnews.Activity.GmailActivity.fb_profile_name;
import static net.qsoft.breakingnews.Activity.GmailActivity.fbphotoUrl;
import static net.qsoft.breakingnews.Activity.GmailActivity.personName;
import static net.qsoft.breakingnews.Activity.GmailActivity.personPhotoUrl;
import static net.qsoft.breakingnews.Service.NotificationService.MyPREFERENCES;
import static net.qsoft.breakingnews.app.AppController.getContext;

public class CategoryDecoratedActivity extends AppCompatActivity {

    private LinearLayoutManager layoutManager;
    MessageListAdapter adapter;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout swipeRefresh;
    ArrayList<Message> messagesArrayList;
    ArrayList<Category> categoryArrayList;
    private RelativeLayout mainLayout, emptyLayout;
    private SearchView searchView;
    private static String key;

    String c2, c3;
    private String category, title, id, category_local;
    private int status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_decorated);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        //toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Category Name");
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        messagesArrayList = new ArrayList<>();
        categoryArrayList = new ArrayList<Category>();


        mRecyclerView = findViewById(R.id.product_list_recycler_view);
        swipeRefresh = findViewById(R.id.swipe_refresh_layout);
        mainLayout = findViewById(R.id.main_layout);
        emptyLayout = findViewById(R.id.empty_layout);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL));
        swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimaryDark));
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // loadMessageList();
                loadMessageList1(category);
            }
        });

        DAO da = new DAO(this);
        da.open();

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                id = null;
               // title = null;
                category = null;
               // status = Integer.parseInt(null);
                getSupportActionBar().setTitle("Message");

            } else {
                id = extras.getString("id");
                category = extras.getString("category");
               // title = extras.getString("title");
               // status = extras.getInt("status");
                //status = 1;

                DAO dao = new DAO(AppController.getInstance());
                dao.open();
                dao.UpdateStatus(status, id);
                dao.close();

                loadMessageList1(category);



//                for (int j = 0; j < da.getAllCategory().size(); j++) {
//                    c3 = da.getAllCategory().get(j).getCategory();
//                    if (c3.equals(category)) {
//                        loadMessageList1(category);
//                    }
//                }



//                switch (category) {
//                    case "Traffic":
//                        Toast.makeText(getApplicationContext(), "Traffic", Toast.LENGTH_LONG).show();
//                        loadMessageList1(category);
//                        break;
//                    case "Sports":
//                        Toast.makeText(getApplicationContext(), "Sports", Toast.LENGTH_LONG).show();
//                        loadMessageList1(category);
//                        break;
//                    case "Entertainment":
//                        Toast.makeText(getApplicationContext(), "Entertainment", Toast.LENGTH_LONG).show();
//                        loadMessageList1(category);
//                        break;
//                    case "Economics":
//                        Toast.makeText(getApplicationContext(), "Economics", Toast.LENGTH_LONG).show();
//                        loadMessageList1(category);
//                        break;
//                    case "Tech News":
//                        Toast.makeText(getApplicationContext(), "Tech News", Toast.LENGTH_LONG).show();
//                        loadMessageList1(category);
//                        break;
//                    case "Life & Style":
//                        Toast.makeText(getApplicationContext(), "Life style", Toast.LENGTH_LONG).show();
//                        loadMessageList1(category);
//                        break;
//                    case "Jobs":
//                        Toast.makeText(getApplicationContext(), "Jobs", Toast.LENGTH_LONG).show();
//                        loadMessageList1(category);
//                        break;
//                    case "Politics":
//                        Toast.makeText(getApplicationContext(), "Politics", Toast.LENGTH_LONG).show();
//                        loadMessageList1(category);
//                        break;
//                    case "International":
//                        Toast.makeText(getApplicationContext(), "International", Toast.LENGTH_LONG).show();
//                        loadMessageList1(category);
//                        break;
//                    default:
//                        Toast.makeText(getApplicationContext(), "Something wrong!", Toast.LENGTH_LONG).show();
//                        break;
//                }

                getSupportActionBar().setTitle(category);

            }
        }

    }

    private void loadMessageList1(String category) {
        DAO dao = new DAO(AppController.getInstance());
        dao.open();
        messagesArrayList = dao.getAllCategoryGroupMessage(category);
        if (!messagesArrayList.isEmpty()) {
            adapter = new MessageListAdapter(this, messagesArrayList);
            mRecyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setAdapter(adapter);
            swipeRefresh.setRefreshing(false);
            emptyLayout.setVisibility(View.GONE);
        } else {
            emptyLayout.setVisibility(View.VISIBLE);
            swipeRefresh.setRefreshing(false);
        }
        dao.close();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                adapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goToHome();
    }


    public void goToHome() {
        SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences(
                MyPREFERENCES, 0);

        String key1 = sharedpreferences.getString("flag", key);

        if (key1.equals("1")) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("personPhotoUrl", personPhotoUrl);
            intent.putExtra("personName", personName);
            intent.putExtra("email", email);
            intent.putExtra("key", 1);

            startActivity(intent);
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
            finish();
        } else {

            Intent goToFbUserActivity = new Intent(this, MainActivity.class);
            //goToFbUserActivity.putExtra("EMAIL", email);
            goToFbUserActivity.putExtra("NAME", fb_profile_name);
            goToFbUserActivity.putExtra("ID", fb_id);
            // goToFbUserActivity.putExtra("GENDER", gender);
            goToFbUserActivity.putExtra("PHOTO_URL", fbphotoUrl);
            goToFbUserActivity.putExtra("key", 5);

            startActivity(goToFbUserActivity);
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
            finish();
        }
    }

}
