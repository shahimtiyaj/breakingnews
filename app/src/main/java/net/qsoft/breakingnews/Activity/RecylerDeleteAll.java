package net.qsoft.breakingnews.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.qsoft.breakingnews.Adapter.RecylerDeleteAllAdapter;
import net.qsoft.breakingnews.app.AppController;
import net.qsoft.breakingnews.Database.DAO;
import net.qsoft.breakingnews.Model.Message;
import net.qsoft.breakingnews.R;
import net.qsoft.breakingnews.helper.DividerItemDecoration;

import java.util.ArrayList;

import static net.qsoft.breakingnews.Activity.GmailActivity.email;
import static net.qsoft.breakingnews.Activity.GmailActivity.fb_id;
import static net.qsoft.breakingnews.Activity.GmailActivity.fb_profile_name;
import static net.qsoft.breakingnews.Activity.GmailActivity.fbphotoUrl;
import static net.qsoft.breakingnews.Activity.GmailActivity.maxGlowRadius;
import static net.qsoft.breakingnews.Activity.GmailActivity.minGlowRadius;
import static net.qsoft.breakingnews.Activity.GmailActivity.personName;
import static net.qsoft.breakingnews.Activity.GmailActivity.personPhotoUrl;
import static net.qsoft.breakingnews.Activity.GmailActivity.startGlowRadius;
import static net.qsoft.breakingnews.Service.NotificationService.MyPREFERENCES;

public class RecylerDeleteAll extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private CheckBox chk_select_all;
    private Button btn_delete_all;
    private SwipeRefreshLayout swipeRefresh;


    private ArrayList<Message> messagesArrayList = new ArrayList<>();
    private RecylerDeleteAllAdapter adapter;

    private static String key;
    Dialog myDialog;
    private View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recyler_check_delete_main);
        myDialog = new Dialog(this);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_delete);
        swipeRefresh = findViewById(R.id.swipe_refresh_layout_delete);

        messagesArrayList = new ArrayList<>();
        adapter = new RecylerDeleteAllAdapter(this, messagesArrayList);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL));
        swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimaryDark));
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadMessageList();
            }
        });
        loadMessageList();

    }


    private void loadMessageList() {


        chk_select_all = (CheckBox) findViewById(R.id.chk_select_all);
        btn_delete_all = (Button) findViewById(R.id.btn_delete_all);


        final DAO dao = new DAO(AppController.getInstance());
        dao.open();
        messagesArrayList = dao.getAllMessage();

        adapter = new RecylerDeleteAllAdapter(getApplicationContext(), messagesArrayList);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(adapter);
        swipeRefresh.setRefreshing(false);


        chk_select_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (chk_select_all.isChecked()) {

                    for (Message model : messagesArrayList) {
                        model.setSelected(true);
                    }
                } else {

                    for (Message model : messagesArrayList) {
                        model.setSelected(false);
                    }
                }

                adapter.notifyDataSetChanged();
            }
        });

        btn_delete_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (chk_select_all.isChecked()) {

                    //Alert();
                    ShowPopup(view);
                    dao.close();
                    //messagesArrayList.clear();
                    adapter.notifyDataSetChanged();
                    chk_select_all.setChecked(false);
                } else {
                    Snackbar.make(v, "Please click on select all check box, to delete all items.", Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences(
                MyPREFERENCES, 0);

        String key1 = sharedpreferences.getString("flag", key);

        if (key1.equals("1")) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("personPhotoUrl", personPhotoUrl);
            intent.putExtra("personName", personName);
            intent.putExtra("email", email);
            intent.putExtra("key", 1);

            startActivity(intent);
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
            finish();
        } else {

            Intent goToFbUserActivity = new Intent(this, MainActivity.class);
            //goToFbUserActivity.putExtra("EMAIL", email);
            goToFbUserActivity.putExtra("NAME", fb_profile_name);
            goToFbUserActivity.putExtra("ID", fb_id);
            // goToFbUserActivity.putExtra("GENDER", gender);
            goToFbUserActivity.putExtra("PHOTO_URL", fbphotoUrl);
            goToFbUserActivity.putExtra("key", 5);

            startActivity(goToFbUserActivity);
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
            finish();
        }
    }



    public void Alert() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Attention Please !")
                .setMessage("Are you sure you want to delete all message!")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DAO dao = new DAO(AppController.getInstance());
                        dao.open();
                        dao.deleteAllMessage();
                        dao.close();
                        Toast.makeText(getApplicationContext(), "All Message Deleted!", Toast.LENGTH_LONG).show();

                    }

                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                }).show();
    }


    public void ShowPopup(View view) {

        Button ok, back;
        TextView txtclose,mItemSelected,alertMessage,attention;

        myDialog.setContentView(R.layout.custom_pop_up);
        LinearLayout layone= (LinearLayout) myDialog.findViewById(R.id.linear_item);
        layone.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        //layone.setVisibility(View.GONE);
        ok = (Button) myDialog.findViewById(R.id.ok);
        ok.setText("OK");
        ok.setTextSize(25);
        ok.setTextColor(getResources().getColor(R.color.white));
        ok.setBackground(null);
        back = (Button) myDialog.findViewById(R.id.back);
        back.setVisibility(View.GONE);
        mItemSelected = (TextView) myDialog.findViewById(R.id.tvItemSelected1);
        mItemSelected.setText("Are you sure you want to delete all message?");

        alertMessage = (TextView) myDialog.findViewById(R.id.alertMessage);
        alertMessage.setVisibility(View.INVISIBLE);


        mItemSelected = (TextView) myDialog.findViewById(R.id.attention);
        mItemSelected.setText("Attention Please!");

        /**
         * glowing code
         */
        // Start Glowing :D
        GlowingText glowText = new GlowingText(this, // Pass activity Object
                getBaseContext(), // Context
                mItemSelected, // TextView
                minGlowRadius, // Minimum Glow Radius
                maxGlowRadius, // Maximum Glow Radius
                startGlowRadius, // Start Glow Radius - Increases to
                // MaxGlowRadius then decreases to
                // MinGlowRadius.
                Color.BLUE, // Glow Color (int)
                1);

        glowText.setGlowColor(Color.RED); // (int : 0xFFffffff)
        glowText.setStartGlowRadius(5f);
        glowText.setMaxGlowRadius(15f);
        glowText.setMinGlowRadius(3f);


        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DAO dao = new DAO(AppController.getInstance());
                dao.open();
                dao.deleteAllMessage();
                dao.close();
                Toast.makeText(getApplicationContext(), "All Message Deleted!", Toast.LENGTH_LONG).show();
                myDialog.dismiss();

                // Toast.makeText(getApplicationContext(), "Sending Data to server..", Toast.LENGTH_LONG).show();
            }
        });

//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                categorySelection();
//            }
//        });

//        back = (TextView) myDialog.findViewById(R.id.back);
//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                categorySelection();            }
//        });

        txtclose = (TextView) myDialog.findViewById(R.id.txtclose);
        txtclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();
    }
}



