package net.qsoft.breakingnews.Activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;

import net.qsoft.breakingnews.Database.DAO;
import net.qsoft.breakingnews.Model.ActiveUser;
import net.qsoft.breakingnews.app.AppController;
import net.qsoft.breakingnews.NetworkService.Utils;
import net.qsoft.breakingnews.NetworkService.VolleyCustomRequest;
import net.qsoft.breakingnews.R;
import net.qsoft.breakingnews.utils.CloudRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;

import io.fabric.sdk.android.Fabric;
import steelkiwi.com.library.view.ErrorContainer;

import static net.qsoft.breakingnews.Database.DBHelper.TABLE_USERINFO;
import static net.qsoft.breakingnews.Service.NotificationService.MyPREFERENCES;
import static net.qsoft.breakingnews.app.AppController.defaultImageUrl;

public class GmailActivity extends AppCompatActivity implements
        View.OnClickListener,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = GmailActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 007;

    private GoogleApiClient mGoogleApiClient;
    private ProgressDialog mProgressDialog;

    private SignInButton gmail_login;
    private Button btnSignOut, btnRevokeAccess, sign_up_later;
    private LinearLayout llProfileLayout;
    private ImageView imgProfilePic;
    private TextView txtName, txtEmail;


    public static String personName="";
    public static String personPhotoUrl;
    public static String email="";

    //facebook
    private LoginButton mLoginButton;
    //private Button mLoginButton;
    private CallbackManager callbackManager;
    private ErrorContainer errorContainer;
    private View view;

    public static String fb_profile_name, fbphotoUrl;
    public static long fb_id;

    @SuppressLint("StaticFieldLeak")
    public static GlowingText glowText, glowText2;
    public static float startGlowRadius = 25f, minGlowRadius = 3f, maxGlowRadius = 15f;

    // Shared Preferences
    SharedPreferences sp;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    private static String key;

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // FacebookSdk.sdkInitialize(getApplicationContext());
        //AppEventsLogger.activateApp(this);
        //requestFullScreenWindow();
        setContentView(R.layout.gmail);
        AnimateText();
        //assign call back manager
        callbackManager = CallbackManager.Factory.create();

        //Fabric io
        Fabric.with(this, new Crashlytics());

        // sp = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        // sp = getSharedPreferences("login",MODE_PRIVATE);


        //find the login button
        mLoginButton = (LoginButton) findViewById(R.id.facebook_login);

        logInCallBackRegister();
        mLoginButton.setReadPermissions(Arrays.asList("public_profile", "email"));


        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            noInternetConnection();
        }

        gmail_login = (SignInButton) findViewById(R.id.gmail_login);

       // sign_up_later = (Button) findViewById(R.id.sign_up_later1);

        gmail_login.setOnClickListener(this);
       // sign_up_later.setOnClickListener(this);
        //btnRevokeAccess.setOnClickListener(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        // Customizing G+ button
        gmail_login.setSize(SignInButton.SIZE_STANDARD);
        gmail_login.setScopes(gso.getScopeArray());
    }


    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.e(TAG, "display name: " + acct.getDisplayName());

//            if (personPhotoUrl == null) {
//               // personPhotoUrl = acct.getPhotoUrl().toString();
//                personName = acct.getDisplayName();
//                email = acct.getEmail();
//                Log.e(TAG, "Name: " + personName + ", email: " + email
//                        + ", Image: " + personPhotoUrl);
//
//                updateUI(true);
//
//            }
//            else {
            personPhotoUrl = acct.getPhotoUrl().toString();
            personName = acct.getDisplayName();
            email = acct.getEmail();
            DAO.executeSQL("INSERT OR REPLACE INTO " + TABLE_USERINFO +
                    "(FbProfileName, FbId, GmailProfileName, Gmail, fbphotoUrl) " +
                    "VALUES(?, ?, ?, ?, ?)", new String[]{"", "", personName, email, ""});


            updateUI(true);
            SendingGmailFbInfoToServer2();
            Log.e(TAG, "Name: " + personName + ", email: " + email
                    + ", Image: " + personPhotoUrl);
            // }
        } else {
            // Signed out, show unauthenticated UI.
            updateUI(false);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {

            case R.id.gmail_login:
                if (Utils.isNetworkAvailable(getApplicationContext())) {
                    showProgressDialog();
                    signIn();

                    // SendingGmailFbInfoToServer1();

                } else {

                    noInternetConnection();
                }

                break;

//            case R.id.sign_up_later1:
//                Intent in = new Intent(getApplicationContext(), MainActivity.class);
//                in.putExtra("key", "2");
//                startActivity(in);
//                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else
            callbackManager.onActivityResult(requestCode, resultCode, data);

//        if (resultCode == RESULT_OK) {
//            Intent goToFbUserActivity = new Intent(this, MainActivity.class);
//            //goToFbUserActivity.putExtra("EMAIL", email);
//            goToFbUserActivity.putExtra("NAME", fb_profile_name);
//            goToFbUserActivity.putExtra("ID", fb_id);
//            // goToFbUserActivity.putExtra("GENDER", gender);
//            goToFbUserActivity.putExtra("PHOTO_URL", fbphotoUrl);
//            goToFbUserActivity.putExtra("key", 5);
//
//            startActivity(goToFbUserActivity);
//            overridePendingTransition(R.anim.right_in, R.anim.right_out);
//            finish();
//
//        } else {
//            Toast.makeText(getApplicationContext(), "Unable to login please check your internet connection",Toast.LENGTH_LONG).show();
//        }
    }

    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            //showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    private void updateUI(boolean isSignedIn) {
        if (isSignedIn) {
            Intent main = new Intent(GmailActivity.this, MainActivity.class);
            if (personPhotoUrl == null) {
                main.putExtra("personPhotoUrl", defaultImageUrl);
            } else {
                main.putExtra("personPhotoUrl", personPhotoUrl);
            }            main.putExtra("personName", personName);
            main.putExtra("email", email);
            main.putExtra("key", "1");
            startActivity(main);
            overridePendingTransition(R.anim.left_in, R.anim.left_out);
            finish();

        } else {
            gmail_login.setVisibility(View.VISIBLE);
        }
    }


    private void requestFullScreenWindow() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isLoggedIn() && Utils.isNetworkAvailable(getApplicationContext())) {

            ActiveUser usr = ActiveUser.getInstance();
            String fbName = usr.getFbProfileName();
            Long fbid = usr.getFbId();
            String fbimageUrl = usr.getPhotoUrl();
            String Ucategory = usr.getUcategory();


            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.putExtra("key", 5);
            intent.putExtra("NAME", fbName);
            intent.putExtra("PHOTO_URL", fbimageUrl);

            SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences(
                    MyPREFERENCES, 0);
            String key1 = sharedpreferences.getString("flag", key);


            startActivity(intent);
            overridePendingTransition(R.anim.left_in, R.anim.left_out);
            finish();

            //  goToViewUserProfile(fbName, fbid, fbimageUrl);
        }
    }

    //facebook-------------------------

    private void logInCallBackRegister() {

        // Callback registration
        mLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                //use profile tracker to get user public info, except email and gender which you will have to use graph request
                // getFbInfoWithProfileTracker();
                //we use this to get user email as well as the rest of the info also included using the profile tracker
                getUserProfileWithGraphRequest(loginResult.getAccessToken());

            }

            @Override
            public void onCancel() {
                // App code
                Toast.makeText(GmailActivity.this, "login canceled by user", Toast.LENGTH_SHORT).show();
            }


            @Override
            public void onError(FacebookException exception) {
                // App code
                Toast.makeText(GmailActivity.this, "Error " + exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * get public and private data like fetch email by requesting user permissions
     **/
    private void getUserProfileWithGraphRequest(AccessToken accessToken) {
        //Toast.makeText(MainActivity.this,"Please Wait...",Toast.LENGTH_SHORT).show();

        GraphRequest request = GraphRequest.newMeRequest(accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {

                            // String email_id = object.getString("email");
                            //String gender = object.getString("gender");
                            fb_profile_name = object.getString("name");
                            fb_id = object.getLong("id");
                            //get photo URL and convert to string, make sure you use https as facebook photo with android can cause problems
                            URL img_value = new URL("https://graph.facebook.com/" + fb_id + "/picture?type=large");
                            fbphotoUrl = img_value.toString();

                            goToViewUserProfile(fb_profile_name, fb_id, fbphotoUrl);

//
//                            DAO.executeSQL("INSERT OR REPLACE INTO " + TABLE_USERINFO + "(FbProfileName, FbId, fbphotoUrl) " +
//                                    "VALUES(?, ?, ?)", new String[]{fb_profile_name, String.valueOf(fb_id), fbphotoUrl});
//

                            // goToViewUserProfileActivity(email_id, profile_name, fb_id, gender, photoUrl);

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            Toast.makeText(GmailActivity.this, "Error " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                            Toast.makeText(GmailActivity.this, "Error " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }

                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, name, email, gender");
        request.setParameters(parameters);
        request.executeAsync();
    }


    private void goToViewUserProfile(String name, long id, String photoUrl) {

        Intent goToFbUserActivity = new Intent(this, MainActivity.class);
        //goToFbUserActivity.putExtra("EMAIL", email);
        goToFbUserActivity.putExtra("NAME", name);
        goToFbUserActivity.putExtra("ID", id);
        // goToFbUserActivity.putExtra("GENDER", gender);
        goToFbUserActivity.putExtra("PHOTO_URL", photoUrl);
        goToFbUserActivity.putExtra("key", 5);


        DAO.executeSQL("INSERT OR REPLACE INTO " + TABLE_USERINFO +
                "(FbProfileName, FbId, GmailProfileName, Gmail, fbphotoUrl) " +
                "VALUES(?, ?, ?, ?, ?)", new String[]{name, String.valueOf(id), "", "", photoUrl});

        startActivity(goToFbUserActivity);


        SendingGmailFbInfoToServer1();

        overridePendingTransition(R.anim.left_in, R.anim.left_out);
        finish();
    }


    public void noInternetConnection() {
        //network not available custom toast---
        Context context = getApplicationContext();
        LayoutInflater inflater = getLayoutInflater();
        View customToastroot = inflater.inflate(R.layout.network_no_connection, null);
        Toast customtoast = new Toast(context);
        customtoast.setView(customToastroot);
        customtoast.setGravity(Gravity.BOTTOM | Gravity.CENTER, 0, 0);
        customtoast.setDuration(Toast.LENGTH_LONG);
        customtoast.show();
    }

    public void AnimateText() {

        TextView myText = (TextView) findViewById(R.id.appname);
        TextView wlcomeSignIn = (TextView) findViewById(R.id.welcomeSignedIn);

        //wlcomeSignIn.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.flash_leave_now));

//        Animation anim = new AlphaAnimation(0.0f, 1.0f);
//        anim.setDuration(50); //You can manage the blinking time with this parameter
//        anim.setStartOffset(20);
//        anim.setRepeatMode(Animation.REVERSE);
//        anim.setRepeatCount(Animation.INFINITE);
//        myText.startAnimation(anim);

        /*
         * glowing code
         */
        //Start Glowing :D
        glowText = new GlowingText(this, // Pass activity Object
                getBaseContext(), // Context
                myText, // TextView
                minGlowRadius, // Minimum Glow Radius
                maxGlowRadius, // Maximum Glow Radius
                startGlowRadius, // Start Glow Radius - Increases to
                // MaxGlowRadius then decreases to
                // MinGlowRadius.
                Color.BLUE, // Glow Color (int)
                1);

        glowText.setGlowColor(Color.RED); // (int : 0xFFffffff)
        glowText.setStartGlowRadius(5f);
        glowText.setMaxGlowRadius(15f);
        glowText.setMinGlowRadius(3f);
    }

    public void SendingGmailFbInfoToServer1() {

        //String hitURL = "http://192.168.0.117/JeneRakhun/public/userinformation";
       // String hitURL = "http://qsoft.net/khobor/public/userinformation";
        //String hitURL = "http://192.168.0.17:8080/khobor/public/userinformation";
        String hitURL = AppController.getUserInfoUrl();

        ActiveUser usr = ActiveUser.getInstance();
        String fbName = usr.getFbProfileName();
        String fbid = String.valueOf(usr.getFbId());
        String Ucategory = usr.getUcategory();

        // String gmailProfile = usr.getGmailProfileName();
        // String gmail = usr.getGmail();
        String gmailProfile = personName;
        String gmail = email;

        String fbPName=fb_profile_name;
        String fbuid=fbid;

        HashMap<String, String> params = new HashMap<>();
        params.put("FbProfileName", fb_profile_name);
        params.put("FbId", String.valueOf(fb_id));
        params.put("GmailProfileName", " ");
        params.put("Gmail", " ");


//        String fb = "আতিক হাসান";
//        String fbid = "45348056700";
//        String gmail = "shah@dddii";
//        String gmid = "4534507599";
//
//        HashMap<String, String> params = new HashMap<>();
//        params.put("FbProfileName", fb);
//        params.put("FbId", fbid);
//        params.put("GmailProfileName", gmail);
//        params.put("Gmail", gmid);
//

        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());

        VolleyCustomRequest postRequest = new VolleyCustomRequest(Request.Method.POST, hitURL, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "Server Response: " + response.toString());

                        try {
                            Log.v("Response:%n %s", response.toString(4));
                            String status = response.getString("status");
                            Log.d("Message", status);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.d(TAG, "Error: " + volleyError.getMessage());

                        if (volleyError instanceof NetworkError) {
                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();
                        } else if (volleyError instanceof ServerError) {
                            Toast.makeText(getApplicationContext(), "The server could not be found. Please try again after some time!!", Toast.LENGTH_SHORT).show();

                        } else if (volleyError instanceof AuthFailureError) {
                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();
                        }

//                         else if (volleyError instanceof ParseError) {
//                            Toast.makeText(getApplicationContext(), "Parsing error! Please try again after some time!!", Toast.LENGTH_SHORT).show();
//
//                        }
                       else if (volleyError instanceof NoConnectionError) {
                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();

                        } else if (volleyError instanceof TimeoutError) {
                            Toast.makeText(getApplicationContext(), "Connection TimeOut! Please check your internet connection", Toast.LENGTH_SHORT).show();

                        }

                        try {
                            Log.e("myerror", "deal first", volleyError);
                            Log.e("myerror", "deal success" + new String(volleyError.networkResponse.data, "utf-8"));
                        } catch (Throwable e) {
                            Log.e("myerror", "deal fail", e);
                        }
                    }
                })

//        {
//            /**
//             * Passing some request headers
//             */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//
//                headers.put("FbProfileName", fb_profile_name);
//                headers.put("FbId", String.valueOf(fb_id));
//                headers.put("GmailProfileName", personName);
//                headers.put("Gmail", email);
//
//                return headers;
//            }
//
//        }

                ;

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        mRequestQueue.add(postRequest);

        // Volley.newRequestQueue(this).add(postRequest);

        CloudRequest.getInstance(this).addToRequestQueue(postRequest);
    }


    public  void SendingGmailFbInfoToServer2(){
        //String hitURL = "http://192.168.0.117/JeneRakhun/public/userinformation";
        // String hitURL = "http://qsoft.net/khobor/public/userinformation";
        //String hitURL = "http://192.168.0.17:8080/khobor/public/userinformation";
        String hitURL = AppController.getUserInfoUrl();

        ActiveUser usr = ActiveUser.getInstance();
        String fbName = usr.getFbProfileName();
        String fbid = String.valueOf(usr.getFbId());
         String gmailProf = usr.getGmailProfileName();
        // String gmail = usr.getGmail();
        String Ucategory = usr.getUcategory();

        String gmailProfile = personName;
        String gmail = email;


        HashMap<String, String> params = new HashMap<>();
        params.put("FbProfileName", " ");
        params.put("FbId", " ");
        params.put("GmailProfileName", gmailProfile);
        params.put("Gmail", gmail);


//        String fb = "আতিক হাসান";
//        String fbid = "45348056700";
//        String gmail = "shah@dddii";
//        String gmid = "4534507599";
//
//        HashMap<String, String> params = new HashMap<>();
//        params.put("FbProfileName", fb);
//        params.put("FbId", fbid);
//        params.put("GmailProfileName", gmail);
//        params.put("Gmail", gmid);
//

        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());

        VolleyCustomRequest postRequest = new VolleyCustomRequest(Request.Method.POST, hitURL, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "Server Response: " + response.toString());

                        try {
                            Log.v("Response:%n %s", response.toString(4));
                            String status = response.getString("status");
                            Log.d("Message", status);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.d(TAG, "Error: " + volleyError.getMessage());

                        if (volleyError instanceof NetworkError) {
                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();
                        } else if (volleyError instanceof ServerError) {
                            Toast.makeText(getApplicationContext(), "The server could not be found. Please try again after some time!!", Toast.LENGTH_SHORT).show();

                        } else if (volleyError instanceof AuthFailureError) {
                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();
                        }

//                        else if (volleyError instanceof ParseError) {
//                            Toast.makeText(getApplicationContext(), "Parsing error! Please try again after some time!!", Toast.LENGTH_SHORT).show();
//
//                        }
                        else if (volleyError instanceof NoConnectionError) {
                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();

                        } else if (volleyError instanceof TimeoutError) {
                            Toast.makeText(getApplicationContext(), "Connection TimeOut! Please check your internet connection", Toast.LENGTH_SHORT).show();

                        }

                        try {
                            Log.e("myerror", "deal first", volleyError);
                            Log.e("myerror", "deal success" + new String(volleyError.networkResponse.data, "utf-8"));
                        } catch (Throwable e) {
                            Log.e("myerror", "deal fail", e);
                        }
                    }
                })

//        {
//            /**
//             * Passing some request headers
//             */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//
//                headers.put("FbProfileName", fb_profile_name);
//                headers.put("FbId", String.valueOf(fb_id));
//                headers.put("GmailProfileName", personName);
//                headers.put("Gmail", email);
//
//                return headers;
//            }
//
//        }

                ;

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        mRequestQueue.add(postRequest);

        // Volley.newRequestQueue(this).add(postRequest);

        CloudRequest.getInstance(this).addToRequestQueue(postRequest);
    }

}
