package net.qsoft.breakingnews.Activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;


import net.qsoft.breakingnews.Adapter.MessageListAdapter;
import net.qsoft.breakingnews.app.AppController;
import net.qsoft.breakingnews.Database.DAO;
import net.qsoft.breakingnews.Model.Message;
import net.qsoft.breakingnews.R;
import net.qsoft.breakingnews.helper.DividerItemDecoration;

import java.util.ArrayList;

import static net.qsoft.breakingnews.Activity.GmailActivity.email;
import static net.qsoft.breakingnews.Activity.GmailActivity.fb_id;
import static net.qsoft.breakingnews.Activity.GmailActivity.fb_profile_name;
import static net.qsoft.breakingnews.Activity.GmailActivity.fbphotoUrl;
import static net.qsoft.breakingnews.Activity.GmailActivity.personName;
import static net.qsoft.breakingnews.Activity.GmailActivity.personPhotoUrl;
import static net.qsoft.breakingnews.Service.NotificationService.MyPREFERENCES;

public class MessageActivity extends AppCompatActivity  {

    private LinearLayoutManager layoutManager;
    MessageListAdapter adapter;
   // CategoryDialogue adapter;

    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout swipeRefresh;
    ArrayList<Message> messagesArrayList;
    ArrayList<Message> messagesArrayList2;
    private int count = 11;

    private RelativeLayout mainLayout, emptyLayout;
    private SearchView searchView;
    private static String key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestFullScreenWindow();
        setContentView(R.layout.activity_message);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
      //  toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("All Notifications");
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        //toolbar.setTitle("All Notifications");

//        whiteNotificationBar(mRecyclerView);
        messagesArrayList = new ArrayList<>();
        adapter = new MessageListAdapter(this, messagesArrayList);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mRecyclerView = findViewById(R.id.product_list_recycler_view);
        swipeRefresh = findViewById(R.id.swipe_refresh_layout);
        mainLayout = findViewById(R.id.main_layout);
        emptyLayout = findViewById(R.id.empty_layout);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL));

        swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimaryDark));
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadMessageList();
            }
        });
        loadMessageList();
    }

    private void loadMessageList() {

        DAO dao = new DAO(AppController.getInstance());
        dao.open();
        messagesArrayList = dao.getAllMessage();


        if (!messagesArrayList.isEmpty()) {
            adapter = new MessageListAdapter(this, messagesArrayList);
            mRecyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setAdapter(adapter);
            swipeRefresh.setRefreshing(false);
            emptyLayout.setVisibility(View.GONE);
        } else {
            emptyLayout.setVisibility(View.VISIBLE);
            swipeRefresh.setRefreshing(false);
        }
        dao.close();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                adapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences(
                MyPREFERENCES, 0);
        String key1 = sharedpreferences.getString("flag", key);
        // close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        } else if (key1.equals("1")) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("personPhotoUrl", personPhotoUrl);
            intent.putExtra("personName", personName);
            intent.putExtra("email", email);
            intent.putExtra("key", 1);

            startActivity(intent);
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
            finish();
        } else {

            Intent goToFbUserActivity = new Intent(this, MainActivity.class);
            //goToFbUserActivity.putExtra("EMAIL", email);
            goToFbUserActivity.putExtra("NAME", fb_profile_name);
            goToFbUserActivity.putExtra("ID", fb_id);
            // goToFbUserActivity.putExtra("GENDER", gender);
            goToFbUserActivity.putExtra("PHOTO_URL", fbphotoUrl);
            goToFbUserActivity.putExtra("key", 5);

            startActivity(goToFbUserActivity);
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
            finish();
        }

        super.onBackPressed();
    }

    private void whiteNotificationBar(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = view.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            view.setSystemUiVisibility(flags);
            getWindow().setStatusBarColor(Color.WHITE);
        }
    }

    private void requestFullScreenWindow() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }


    /**
     * chooses a random color from array.xml
     */
    private int getRandomMaterialColor(String typeColor) {
        int returnColor = Color.GRAY;
        int arrayId = getResources().getIdentifier("mdcolor_" + typeColor, "array", getPackageName());

        if (arrayId != 0) {
            TypedArray colors = getResources().obtainTypedArray(arrayId);
            int index = (int) (Math.random() * colors.length());
            returnColor = colors.getColor(index, Color.GRAY);
            colors.recycle();
        }
        return returnColor;
    }

}
