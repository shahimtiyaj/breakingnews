package net.qsoft.breakingnews.Activity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import net.qsoft.breakingnews.Adapter.SelectMessageAdapter;
import net.qsoft.breakingnews.app.AppController;
import net.qsoft.breakingnews.Database.DAO;
import net.qsoft.breakingnews.Model.Message;
import net.qsoft.breakingnews.R;

import java.util.ArrayList;

public class ArchiveMessage extends AppCompatActivity  implements
        SelectMessageAdapter.messageAdapterListener{
    private LinearLayoutManager layoutManager;
    SelectMessageAdapter adapter;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout swipeRefresh;
    ArrayList<Message> messagesArrayList;
    private RelativeLayout mainLayout, emptyLayout;
    private SearchView searchView;
    SelectMessageAdapter.messageAdapterListener listener;

    private  int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        //getSupportActionBar().setTitle("All Archive Message");

        messagesArrayList = new ArrayList<>();
        adapter = new SelectMessageAdapter(getApplicationContext(), messagesArrayList, this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mRecyclerView = findViewById(R.id.product_list_recycler_view);
        swipeRefresh = findViewById(R.id.swipe_refresh_layout);
        mainLayout = findViewById(R.id.main_layout);
        emptyLayout = findViewById(R.id.empty_layout);
        swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimaryDark));
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadMessageList();
            }
        });
        loadMessageList();
    }

    private void loadMessageList() {
        DAO dao = new DAO(AppController.getInstance());
        dao.open();
        messagesArrayList=dao.getAllMessage();
        if (!messagesArrayList.isEmpty()) {
            adapter = new SelectMessageAdapter(getApplicationContext(), messagesArrayList, listener);
            mRecyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setAdapter(adapter);
            swipeRefresh.setRefreshing(false);
            emptyLayout.setVisibility(View.GONE);
        } else {
            emptyLayout.setVisibility(View.VISIBLE);
            swipeRefresh.setRefreshing(false);
        }
        dao.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_notifications, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int option_id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (option_id == R.id.btn_delete) {

            DAO dao = new DAO(AppController.getInstance());
            dao.open();
            dao.deleteSingleMessage(String.valueOf(id));
            dao.close();
            Toast.makeText(getApplicationContext(), "Deleted!", Toast.LENGTH_LONG).show();


        }

        return super.onOptionsItemSelected(item);
}

    @Override
    public void onBackPressed() {
        // close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }

    private void whiteNotificationBar(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = view.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            view.setSystemUiVisibility(flags);
            getWindow().setStatusBarColor(Color.WHITE);
        }
    }
    @Override
    public void onMessageSelected(Message message) {
        Toast.makeText(getApplicationContext(), "Selected: " + message.getTitle() + ", " + message.getMessage(), Toast.LENGTH_LONG).show();
    }
}
