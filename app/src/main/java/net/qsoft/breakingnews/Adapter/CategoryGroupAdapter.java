package net.qsoft.breakingnews.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.qsoft.breakingnews.Activity.CategoryDecoratedActivity;
import net.qsoft.breakingnews.Activity.DetailsActivity;
import net.qsoft.breakingnews.Activity.RecylerDeleteAll;
import net.qsoft.breakingnews.Database.DAO;
import net.qsoft.breakingnews.Model.Category;
import net.qsoft.breakingnews.Model.Message;
import net.qsoft.breakingnews.R;
import net.qsoft.breakingnews.app.AppController;

import java.util.ArrayList;
import java.util.Random;

import static net.qsoft.breakingnews.app.AppController.getContext;

public class CategoryGroupAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private Context context;
    private ArrayList<Category> categoryArrayList;
    private ArrayList<Category> categoryArrayListFiltered;
    public Button btnArchive;

    private static final int MESSAGE_ITEM_VIEW_TYPE = 1;
    private static final int AD_VIEW_TYPE = 1;
    private static String local_id;
    public int f = 0;

    private int count = 21;
    private static final int TYPE_FOOTER = 2;

    private int star=0;

    public ImageView iconImp, imgProfile;
    SwipeRefreshLayout swiper;

    public CategoryGroupAdapter(Context context, ArrayList<Category> categoryArrayList) {
        this.context = context;
        this.categoryArrayList = categoryArrayList;
        this.categoryArrayListFiltered = categoryArrayList;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == MESSAGE_ITEM_VIEW_TYPE) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.category_group_layout, parent, false);
            return new CategoryGroupAdapter.MessageViewHolder(itemView);
        } else if (viewType == TYPE_FOOTER) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.footer_layout, parent, false);
            return new CategoryGroupAdapter.FooterViewHolder(itemView);
        } else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof CategoryGroupAdapter.MessageViewHolder) {

            CategoryGroupAdapter.MessageViewHolder messageViewHolder = (CategoryGroupAdapter.MessageViewHolder) holder;
            setMessageView(messageViewHolder, position);

        } else if (holder instanceof CategoryGroupAdapter.FooterViewHolder) {
            CategoryGroupAdapter.FooterViewHolder footerHolder = (CategoryGroupAdapter.FooterViewHolder) holder;

            if ((count - 1) == categoryArrayListFiltered.size()) {

                footerHolder.footerText.setText("NO MORE RESULTS TO SHOW");
            } else {

                footerHolder.footerText.setText("LOAD MORE");
            }

            footerHolder.footerText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setMyCount();
                    notifyDataSetChanged();
                }
            });

        }
    }

    @SuppressLint("SetTextI18n")
    private void setMessageView(final CategoryGroupAdapter.MessageViewHolder holder, final int position) {
        final Category categoryGrp = (Category) categoryArrayListFiltered.get(position);
        DAO dao = new DAO(AppController.getInstance());
        dao.open();
        final String category=categoryGrp.getCategory();

        holder.category_group.setText(categoryGrp.getCategory());
        holder.iconText.setText(categoryGrp.getCategory());

        holder.iconText.setText(categoryGrp.getCategory().substring(0, 1));

        Animation animation = AnimationUtils.loadAnimation(getContext(),R.anim.translate);
        animation.setStartOffset(30 * position);//Provide delay here
        holder.itemView.startAnimation(animation);


        holder.iconImp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });


        holder.relative_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, CategoryDecoratedActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("id", categoryArrayList.get(position).getId());
                intent.putExtra("category", categoryArrayList.get(position).getCategory());
                //intent.putExtra("status", categoryArrayList.get(position).getStatus());
                 context.startActivity(intent);
                ((Activity)context).finish();

            }
        });

    }



    @Override
    public int getItemCount() {
        if (categoryArrayListFiltered.size() < 20) {
            return categoryArrayListFiltered.size();
        } else {
            return count;
        }
    }

    public void setMyCount() {
        int remaining = categoryArrayListFiltered.size() - count;
        if (remaining > 20) {
            this.count = count + 20;
        } else {
            this.count = count + remaining + 1;
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        } else {
            return MESSAGE_ITEM_VIEW_TYPE;
        }

    }

    private boolean isPositionFooter(int position) {
        return position == count - 1;
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout layoutRightButtons;
        private TextView id, category_group, title_group_category, date, time, iconText;

        CardView card_view;
        RelativeLayout relative_view;
        public ImageView iconImp;

        public MessageViewHolder(final View itemView) {
            super(itemView);
            layoutRightButtons = itemView.findViewById(R.id.layoutRightButtons);
            card_view = (CardView) itemView.findViewById(R.id.card_view);
            relative_view = (RelativeLayout) itemView.findViewById(R.id.relative_view);

            id = itemView.findViewById(R.id.id);
            category_group = itemView.findViewById(R.id.category_group);
            title_group_category = itemView.findViewById(R.id.title_group_category);
            iconText = (TextView) itemView.findViewById(R.id.icon_text);
            iconImp = (ImageView) itemView.findViewById(R.id.icon_star1);

            Random r = new Random();
            int red=r.nextInt(255 - 0 + 1)+0;
            int green=r.nextInt(255 - 0 + 1)+0;
            int blue=r.nextInt(255 - 0 + 1)+0;

            GradientDrawable draw = new GradientDrawable();
            draw.setShape(GradientDrawable.OVAL);
            draw.setColor(Color.rgb(red,green,blue));
            iconText.setBackground(draw);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                  //  Intent intent = new Intent(context, RecylerDeleteAll.class);
                   // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   // intent.putExtra("category", categoryArrayList.get(position).getCategory());
                    //intent.putExtra("status", categoryArrayList.get(position).getStatus());
                  //  context.startActivity(intent);
                  //  ((Activity)context).finish();
                    return false;
                }
            });

        }

    }


    public void delete(int position) {
        categoryArrayListFiltered.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    categoryArrayListFiltered = categoryArrayList;
                } else {
                    ArrayList<Category> filteredList = new ArrayList<>();
                    for (Category row : categoryArrayList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getCategory().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    categoryArrayListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = categoryArrayListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                categoryArrayListFiltered = (ArrayList<Category>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    class FooterViewHolder extends RecyclerView.ViewHolder {

        TextView footerText;

        public FooterViewHolder(View view) {
            super(view);
            footerText = (TextView) view.findViewById(R.id.footer_text);
        }

    }

    public void Alert(final int position) {
        new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Attention Please !")
                .setMessage("Are you sure you want to delete this message!")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(context, "Deleted!", Toast.LENGTH_LONG).show();

                    }

                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                }).show();
    }

}
