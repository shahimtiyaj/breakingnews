package net.qsoft.breakingnews.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.qsoft.breakingnews.Activity.DetailsActivity;
import net.qsoft.breakingnews.Activity.RecylerDeleteAll;
import net.qsoft.breakingnews.R;
import net.qsoft.breakingnews.app.AppController;
import net.qsoft.breakingnews.Database.DAO;
import net.qsoft.breakingnews.Model.Message;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.StringTokenizer;

import static com.facebook.FacebookSdk.getApplicationContext;
import static net.qsoft.breakingnews.app.AppController.getContext;


public class MessageListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private Context context;
    private ArrayList<Message> messageArrayList;
    private ArrayList<Message> messageArrayListFiltered;
    public Button btnArchive;

    private static final int MESSAGE_ITEM_VIEW_TYPE = 1;
    private static final int AD_VIEW_TYPE = 1;
    private static String local_id;
    public int f = 0;

    private int count = 21;
    private static final int TYPE_FOOTER = 2;
    private static final int TYPE_SECTION = 0;


    //nex3z notification  badge --
    public com.nex3z.notificationbadge.NotificationBadge mBadge;
    public int mCount = 1;

    TextView smsCountTxt;
    int pendingSMSCount = 10;

    private int star = 0;

    public ImageView iconImp, imgProfile;
    SwipeRefreshLayout swiper;

    String convertDate;

    // String datePart;

    public MessageListAdapter(Context context, ArrayList<Message> messageArrayList) {
        this.context = context;
        this.messageArrayList = messageArrayList;
        this.messageArrayListFiltered = messageArrayList;
        //this.swiper = swiper;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        switch (viewType) {
//            case MESSAGE_ITEM_VIEW_TYPE:
//            default:
//
//                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.test, parent, false);
//
//               /* Animation animationY = new TranslateAnimation(
//                        view.getWidth() / 1, 0, view.getHeight() / 2,
//                        0);
//                animationY.setDuration(1000);
//                view.startAnimation(animationY);
//                animationY = null; */
//
//                return new MessageViewHolder(view);
//
//        }


        if (viewType == MESSAGE_ITEM_VIEW_TYPE) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.test, parent, false);
            return new MessageViewHolder(itemView);
        } else if (viewType == TYPE_FOOTER) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.footer_layout, parent, false);
            return new FooterViewHolder(itemView);
        } else if (viewType == TYPE_SECTION) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.section_view_layout, parent, false);
            return new SectionViewHolder(itemView);
        } else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//        int viewType = getItemViewType(position);
//        switch (viewType) {
//            case MESSAGE_ITEM_VIEW_TYPE:
//            default:
//                MessageViewHolder messageViewHolder = (MessageViewHolder) holder;
//                setMessageView(messageViewHolder, position);
//                break;


        if (holder instanceof MessageViewHolder) {

            MessageViewHolder messageViewHolder = (MessageViewHolder) holder;
            setMessageView(messageViewHolder, position);

        } else if (holder instanceof FooterViewHolder) {
            FooterViewHolder footerHolder = (FooterViewHolder) holder;

            if ((count - 1) == messageArrayListFiltered.size()) {

                footerHolder.footerText.setText("NO MORE RESULTS TO SHOW");
            } else {

                //  footerHolder.footerText.setBackgroundResource(R.drawable.ic_more_vert);

                footerHolder.footerText.setText("LOAD MORE");
            }

            footerHolder.footerText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setMyCount();
                    notifyDataSetChanged();
                }
            });

        } else if (holder instanceof SectionViewHolder) {
            SectionViewHolder sectionViewHolder = (SectionViewHolder) holder;
            // sectionViewHolder.section_text.setText("Today");

//            if (datePart=="Sunday, 8th July 2018"){
//                sectionViewHolder.section_text.setText("Today");
//
//            }
//            else {
//                sectionViewHolder.section_text.setText("Yesterday");
//
//            }
        }
    }

    @SuppressLint("SetTextI18n")
    private void setMessageView(final MessageViewHolder holder, final int position) {
        final Message messages = (Message) messageArrayListFiltered.get(position);
        DAO dao = new DAO(AppController.getInstance());
        dao.open();
        final int status = messages.getStatus();
        final String fv_id = messages.getId();
        final String fv_title = messages.getTitle();
        final String fv_message = messages.getMessage();
        final int flag = messages.getFlag();
        final int favourite = messages.getFavourite();
        final String category = messages.getCategory();
        final String time = messages.getTime();

        // String[] separated = time.split(";");
        // String datePart=separated[0].trim();
        // String timePart=separated[1].trim();

        StringTokenizer tokens = new StringTokenizer(time, ";");
        final String datePart = tokens.nextToken().trim();//
        final String timePart = tokens.nextToken().trim();//


        StringTokenizer tokens1 = new StringTokenizer(datePart, ",");
        final String WeekName = tokens1.nextToken().trim();//
        final String DPart2 = tokens1.nextToken().trim();//


        StringTokenizer tokens2 = new StringTokenizer(datePart, " ");
        final String P1 = tokens2.nextToken().trim();//
        final String P2 = tokens2.nextToken().trim();//


//        try {
//            String myDate = datePart;
//            SimpleDateFormat src = new SimpleDateFormat("EEEE, d'th' MMMM yyyy", Locale.ENGLISH);
//            SimpleDateFormat dt = new SimpleDateFormat("EEEE, MMM d, yyyy", Locale.ENGLISH);
//             Date date = src.parse(myDate);
//             convertDate = dt.format(date);
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }

//        SimpleDateFormat format = new SimpleDateFormat("EEEE, d'th' MMMM yyyy", Locale.US);
//        String date = format.format(new Date());

        SimpleDateFormat format = new SimpleDateFormat("EEEE", Locale.US);
        final String Today = format.format(new Date());


        String sDate = Today;
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE", Locale.getDefault());
        Date date1 = null;
        try {
            date1 = dateFormat.parse(sDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date1);
        calendar.add(Calendar.DATE, -1);
        final String Yesterday = dateFormat.format(calendar.getTime());


        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(date1);
        calendar1.add(Calendar.DATE, -2);
        final String twoDaysAgo = dateFormat.format(calendar1.getTime());


        // holder.id.setText(messages.getId());
        holder.title.setText(messages.getTitle());
        holder.message.setText(messages.getMessage());
        holder.iconText.setText(messages.getTitle().substring(0, 1));


        if (WeekName.equals(Today)) {
            holder.time_notify.setText(timePart);
        }
        else {
            if (P2.length() == 4) {
                holder.time_notify.setText(DPart2.substring(0, 9));
            } else {
                holder.time_notify.setText(DPart2.substring(0, 8));
            }
        }


//        if (WeekName.equals(Today)) {
//            holder.iconText.setText("Today");
//        }
//
//        else {
//            if (P2.length() == 4) {
//                holder.iconText.setText(DPart2.substring(0, 9));
//            } else {
//                holder.iconText.setText(DPart2.substring(0, 8));
//            }
//        }


        if (fv_title.length() >= 25) {
            holder.title.setText(messages.getTitle().substring(0, 25) + "...");
        } else {
            holder.title.setText(messages.getTitle());
        }
        if (fv_message.length() >= 32) {
            holder.message.setText(messages.getMessage().substring(0, 32) + "...");
        } else {
            holder.message.setText(messages.getMessage());
        }


        Animation animation = AnimationUtils.loadAnimation(context, R.anim.translate);
        animation.setStartOffset(30 * position);//Provide delay here
        holder.itemView.startAnimation(animation);

        if (status == 0) {
            holder.title.setTypeface(holder.title.getTypeface(), android.graphics.Typeface.BOLD);
            holder.time_notify.setTypeface(holder.time_notify.getTypeface(), android.graphics.Typeface.BOLD);
            holder.time_notify.setTextColor(Color.parseColor("#1E0FB9"));

        }
        if (favourite == 0) {
            //1st try
            //  holder.iconImp.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_border_black_24dp));
            // 2nd try
            //  holder.iconImp.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_border_black_24dp));
            holder.iconImp.setImageResource((R.drawable.ic_star_border_black_24dp));


        } else {
            // holder.iconImp.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_black_24dp));

            //  holder.iconImp.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_black_24dp));

            holder.iconImp.setImageResource((R.drawable.ic_star_black_24dp));


        }


        holder.iconImp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DAO dao = new DAO(AppController.getInstance());
                dao.open();
                dao.UpdateFlag(1, fv_id);

                if (favourite == 0) {
                    //  holder.iconImp.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_black_24dp));
                    holder.iconImp.setImageResource((R.drawable.ic_star_black_24dp));

                    dao.UpdateFavourite(1, fv_id);
                    Toast.makeText(context, "Added to favourite ", Toast.LENGTH_LONG).show();

                    // Toast.makeText(context, fv_title + ":Added to favourite list !", Toast.LENGTH_LONG).show();

                } else {
                    //  holder.iconImp.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_border_black_24dp));
                    holder.iconImp.setImageResource((R.drawable.ic_star_border_black_24dp));

                    dao.UpdateFavourite(0, fv_id);
                    dao.UpdateFlag(0, fv_id);

                    Toast.makeText(context, "Remove from favourite ", Toast.LENGTH_LONG).show();
                }
            }
        });





//        swiper.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                refresh();
//            }
//        });


//        btnArchive.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                DAO dao = new DAO(AppController.getInstance());
//                dao.open();
//                dao.UpdateFlag(1, fv_id);
//
//                if (favourite == 0) {
//                    dao.UpdateFavourite(1, fv_id);
//                    btnArchive.setBackgroundResource(R.drawable.ic_favorite_black_24dp);
//                    //delete(position);
//
//                    Toast.makeText(context, fv_title + ":Added to favourite list !", Toast.LENGTH_LONG).show();
//
//                } else {
//                    dao.UpdateFavourite(0, fv_id);
//                    dao.UpdateFlag(0, fv_id);
//                  //  btnArchive.setBackgroundResource(R.drawable.ic_favorite_border_black_24dp);
//
//                    Toast.makeText(context, fv_title + ":Remove from favourite list !", Toast.LENGTH_LONG).show();
//
//                }
//
//                String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
//
//
//                //   if (flag==1) {
//                //    btnArchive.setClickable(false);
//                //     Toast.makeText(context, "Already Added to favourite list!: " + fv_title, Toast.LENGTH_LONG).show();
//                //     btnArchive.setBackgroundResource(R.drawable.ic_favorite_black_24dp);
//                //    }
//                // else
//
////                DAO.executeSQL("INSERT OR REPLACE INTO " + TABLE_ARCHIVE + "(id, title, message, time) " +
////                                "VALUES(?, ?, ?, ?)",
////
////                        new String[]{fv_id, fv_title, fv_message, currentDateTimeString});
//
//
//                dao.close();
//            }
//        });


        holder.relative_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, DetailsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("id", messageArrayList.get(position).getId());
                intent.putExtra("title", messageArrayList.get(position).getTitle());
                intent.putExtra("message", messageArrayList.get(position).getMessage());
                intent.putExtra("category", messageArrayList.get(position).getCategory());

                intent.putExtra("date", datePart);
                intent.putExtra("time", timePart);

                intent.putExtra("weekName", WeekName);
                intent.putExtra("today", Today);
                intent.putExtra("twodaysAgo", twoDaysAgo);
                intent.putExtra("yesterday", Yesterday);

                intent.putExtra("status", messageArrayList.get(position).getStatus());
                context.startActivity(intent);
                ((Activity) context).finish();

            }
        });

    }

    private void refresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                messageArrayList.add(0, messageArrayList.get(new Random().nextInt(messageArrayList.size())));

                MessageListAdapter.this.notifyDataSetChanged();

                swiper.setRefreshing(false);
            }
        }, 3000);
    }


    @Override
    public int getItemCount() {
        if (messageArrayListFiltered.size() < 20) {
            return messageArrayListFiltered.size();
        } else {
            return count;
        }


        // return messageArrayListFiltered.size();
    }

    public void setMyCount() {
        int remaining = messageArrayListFiltered.size() - count;
        if (remaining > 20) {
            this.count = count + 20;
        } else {
            this.count = count + remaining + 1;
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        }

//        else if(position%8==0){
//           // return position % 2 * 2;
//            return TYPE_SECTION;
//
//        }

        else {
            return MESSAGE_ITEM_VIEW_TYPE + TYPE_SECTION;
        }

        //return MESSAGE_ITEM_VIEW_TYPE;
    }

    private boolean isPositionFooter(int position) {
        return position == count - 1;
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout layoutRightButtons;
        private TextView id, message, title, date, time_notify, iconText;

        CardView card_view;
        RelativeLayout relative_view;
        public ImageView iconImp;

        public MessageViewHolder(final View itemView) {
            super(itemView);
            layoutRightButtons = itemView.findViewById(R.id.layoutRightButtons);
            card_view = (CardView) itemView.findViewById(R.id.card_view);
            relative_view = (RelativeLayout) itemView.findViewById(R.id.relative_view);

            id = itemView.findViewById(R.id.id);
            title = itemView.findViewById(R.id.title);
            message = itemView.findViewById(R.id.message);
            time_notify = itemView.findViewById(R.id.time_notify);
            btnArchive = itemView.findViewById(R.id.btnArchive);
            iconText = (TextView) itemView.findViewById(R.id.icon_text);
            iconImp = (ImageView) itemView.findViewById(R.id.icon_star1);

            //  mBadge = itemView.findViewById(R.id.badge);

            Random r = new Random();
            int red = r.nextInt(255 - 0 + 1) + 0;
            int green = r.nextInt(255 - 0 + 1) + 0;
            int blue = r.nextInt(255 - 0 + 1) + 0;

            GradientDrawable draw = new GradientDrawable();
            draw.setShape(GradientDrawable.OVAL);
            draw.setColor(Color.rgb(red, green, blue));
            iconText.setBackground(draw);


            //Long Press
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    //DatabaseHelper databaseHelper = new DatabaseHelper(context);
                    //local_id = messages.getId();
                    //databaseHelper.deleteSingleMessage(local_id);
                    // Alert(getAdapterPosition());

                    Intent intent = new Intent(context, RecylerDeleteAll.class);
                    context.startActivity(intent);
                    //  ((Activity)context).finish();

                    return false;
                }
            });


//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    // send selected contact in callback
//                    listener.onMessageSelected(messageArrayListFiltered.get(getOldPosition()));
//                }
//            });


        }


    }

    private void applyImportant(MessageViewHolder holder, Message message) {
        if (message.isImportant()) {
            holder.iconImp.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_black_24dp));
            holder.iconImp.setColorFilter(ContextCompat.getColor(context, R.color.icon_tint_selected));
        } else {
            holder.iconImp.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_border_black_24dp));
            holder.iconImp.setColorFilter(ContextCompat.getColor(context, R.color.icon_tint_normal));
        }
    }


    public void delete(int position) {
//        final Message messages = (Message) messageArrayListFiltered.get(position);
//        DAO dao = new DAO(AppController.getInstance());
//        dao.open();
//        local_id = messages.getId();
//        dao.deleteSingleMessage(local_id);

        messageArrayListFiltered.remove(position);
        notifyItemRemoved(position);

        // dao.close();
    }


    private void setupBadge() {

        if (smsCountTxt != null) {
            if (pendingSMSCount == 0) {
                if (smsCountTxt.getVisibility() != View.GONE) {
                    smsCountTxt.setVisibility(View.GONE);
                }
            } else {
                smsCountTxt.setText(String.valueOf(Math.min(pendingSMSCount, 99)));
                if (smsCountTxt.getVisibility() != View.VISIBLE) {
                    smsCountTxt.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    messageArrayListFiltered = messageArrayList;
                } else {
                    ArrayList<Message> filteredList = new ArrayList<>();
                    for (Message row : messageArrayList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getTitle().toLowerCase().contains(charString.toLowerCase()) || row.getMessage().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    messageArrayListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = messageArrayListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                messageArrayListFiltered = (ArrayList<Message>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    class FooterViewHolder extends RecyclerView.ViewHolder {

        TextView footerText;

        public FooterViewHolder(View view) {
            super(view);
            footerText = (TextView) view.findViewById(R.id.footer_text);
        }

    }

    class SectionViewHolder extends RecyclerView.ViewHolder {

        TextView section_text;

        public SectionViewHolder(View view) {
            super(view);
            section_text = (TextView) view.findViewById(R.id.section_text);
        }

    }


    public void Alert(final int position) {
        new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Attention Please !")
                .setMessage("Are you sure you want to delete this message!")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //delete(position);

                        Toast.makeText(context, "Deleted!", Toast.LENGTH_LONG).show();

                    }

                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                }).show();
    }


    private String DateTime() {
        //SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat dt = new SimpleDateFormat("EEEE, d'th' MMMM yyyy", Locale.ENGLISH);

        Date date = new Date();
        return dt.format(date);
        // return DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(date);
        // return DateFormat.getDateTimeInstance().format(new Date());//latest and standard formt
    }



}
