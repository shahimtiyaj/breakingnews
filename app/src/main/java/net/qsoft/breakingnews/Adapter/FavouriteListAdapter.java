package net.qsoft.breakingnews.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.qsoft.breakingnews.Activity.DetailsActivity;
import net.qsoft.breakingnews.Activity.RecylerDeleteAll;
import net.qsoft.breakingnews.app.AppController;
import net.qsoft.breakingnews.Database.DAO;
import net.qsoft.breakingnews.Model.Message;
import net.qsoft.breakingnews.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.StringTokenizer;

public class FavouriteListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private Context context;
    private ArrayList<Message> messageArrayList;
    private ArrayList<Message> messageArrayListFiltered;
    public Button btnArchive;

    private static final int MESSAGE_ITEM_VIEW_TYPE = 1;
    private static final int AD_VIEW_TYPE = 1;
    private static String local_id;
    public int f = 0;

    private int count = 21;
    private static final int TYPE_FOOTER = 2;

    //nex3z notification  badge --
    public com.nex3z.notificationbadge.NotificationBadge mBadge;
    public int mCount = 1;

    TextView smsCountTxt;
    int pendingSMSCount = 10;

    private int star = 0;

    public ImageView iconImp, imgProfile;


    public FavouriteListAdapter(Context context, ArrayList<Message> messageArrayList) {
        this.context = context;
        this.messageArrayList = messageArrayList;
        this.messageArrayListFiltered = messageArrayList;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == MESSAGE_ITEM_VIEW_TYPE) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.test, parent, false);
            return new FavouriteListAdapter.MessageViewHolder(itemView);
        } else if (viewType == TYPE_FOOTER) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.footer_layout, parent, false);
            return new FavouriteListAdapter.FooterViewHolder(itemView);
        } else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof FavouriteListAdapter.MessageViewHolder) {

            FavouriteListAdapter.MessageViewHolder messageViewHolder = (FavouriteListAdapter.MessageViewHolder) holder;
            setMessageView(messageViewHolder, position);

        } else if (holder instanceof FavouriteListAdapter.FooterViewHolder) {
            FavouriteListAdapter.FooterViewHolder footerHolder = (FavouriteListAdapter.FooterViewHolder) holder;

            if ((count - 1) == messageArrayListFiltered.size()) {

                footerHolder.footerText.setText("NO MORE RESULTS TO SHOW");
            } else {

                footerHolder.footerText.setText("LOAD MORE");
            }

            footerHolder.footerText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setMyCount();
                    notifyDataSetChanged();
                }
            });

        }
    }

    @SuppressLint("SetTextI18n")
    private void setMessageView(final FavouriteListAdapter.MessageViewHolder holder, final int position) {
        final Message messages = (Message) messageArrayListFiltered.get(position);
        DAO dao = new DAO(AppController.getInstance());
        dao.open();
        final int status = messages.getStatus();
        final String fv_id = messages.getId();
        final String fv_title = messages.getTitle();
        final String fv_message = messages.getMessage();
        final int flag = messages.getFlag();
        final int favourite = messages.getFavourite();
        final String time = messages.getTime();
        final String category = messages.getCategory();

        // String[] separated = time.split(";");
        // String datePart=separated[0].trim();
        // String timePart=separated[1].trim();

        StringTokenizer tokens = new StringTokenizer(time, ";");
        final String datePart = tokens.nextToken().trim();//
        final String timePart = tokens.nextToken().trim();//


        StringTokenizer tokens1 = new StringTokenizer(datePart, ",");
        final String WeekName = tokens1.nextToken().trim();//
        final String DPart2 = tokens1.nextToken().trim();//


        StringTokenizer tokens2 = new StringTokenizer(datePart, " ");
        final String P1 = tokens2.nextToken().trim();//
        final String P2 = tokens2.nextToken().trim();//


//        try {
//            String myDate = datePart;
//            SimpleDateFormat src = new SimpleDateFormat("EEEE, d'th' MMMM yyyy", Locale.ENGLISH);
//            SimpleDateFormat dt = new SimpleDateFormat("EEEE, MMM d, yyyy", Locale.ENGLISH);
//             Date date = src.parse(myDate);
//             convertDate = dt.format(date);
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }

//        SimpleDateFormat format = new SimpleDateFormat("EEEE, d'th' MMMM yyyy", Locale.US);
//        String date = format.format(new Date());

        SimpleDateFormat format = new SimpleDateFormat("EEEE", Locale.US);
        final String Today = format.format(new Date());


        String sDate = Today;
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE", Locale.getDefault());
        Date date1 = null;
        try {
            date1 = dateFormat.parse(sDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date1);
        calendar.add(Calendar.DATE, -1);
        final String Yesterday = dateFormat.format(calendar.getTime());


        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(date1);
        calendar1.add(Calendar.DATE, -2);
        final String twoDaysAgo = dateFormat.format(calendar1.getTime());


        // holder.id.setText(messages.getId());
        holder.title.setText(messages.getTitle());
        holder.message.setText(messages.getMessage());
        holder.iconText.setText(messages.getTitle().substring(0, 1));


        if (WeekName.equals(Today)) {
            holder.time.setText(timePart);
        }
        else {
            if (P2.length() == 4) {
                holder.time.setText(DPart2.substring(0, 9));
            } else {
                holder.time.setText(DPart2.substring(0, 8));
            }
        }


//        if (WeekName.equals(Today)) {
//            holder.iconText.setText("Today");
//        }
//
//        else {
//            if (P2.length() == 4) {
//                holder.iconText.setText(DPart2.substring(0, 9));
//            } else {
//                holder.iconText.setText(DPart2.substring(0, 8));
//            }
//        }

        if (fv_title.length()>=25) {
            holder.title.setText(messages.getTitle().substring(0,25)+"...");
        }
        else {
            holder.title.setText(messages.getTitle());

        }
        if (fv_message.length()>=30){
            holder.message.setText(messages.getMessage().substring(0, 30)+"...");

        }
        else {
            holder.message.setText(messages.getMessage());
        }

        Animation animation = AnimationUtils.loadAnimation(context, R.anim.translate);
        animation.setStartOffset(30 * position);//Provide delay here
        holder.itemView.startAnimation(animation);

        if (status == 0) {
            holder.title.setTypeface(holder.title.getTypeface(), android.graphics.Typeface.BOLD);

        }
        if (favourite == 0) {

            holder.iconImp.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_border_black_24dp));

        } else {

            holder.iconImp.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_black_24dp));

        }


        holder.iconImp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DAO dao = new DAO(AppController.getInstance());
                dao.open();
                dao.UpdateFlag(1, fv_id);

                if (favourite == 0) {
                    holder.iconImp.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_black_24dp));
                    dao.UpdateFavourite(1, fv_id);
                    Toast.makeText(context,  "Added to favourite ", Toast.LENGTH_LONG).show();

                   // Toast.makeText(context, fv_title + ":Added to favourite list !", Toast.LENGTH_LONG).show();

                } else {
                    holder.iconImp.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_border_black_24dp));
                    dao.UpdateFavourite(0, fv_id);
                    dao.UpdateFlag(0, fv_id);
                    messageArrayListFiltered.remove(position);
                    notifyDataSetChanged();

                    Toast.makeText(context, "Remove from favourite ", Toast.LENGTH_LONG).show();
                }
            }
        });


        holder.relative_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, DetailsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("id", messageArrayList.get(position).getId());
                intent.putExtra("title", messageArrayList.get(position).getTitle());
                intent.putExtra("message", messageArrayList.get(position).getMessage());
                intent.putExtra("category", messageArrayListFiltered.get(position).getCategory());

                intent.putExtra("date", datePart);
                intent.putExtra("time", timePart);

                intent.putExtra("weekName", WeekName);
                intent.putExtra("today", Today);
                intent.putExtra("twodaysAgo", twoDaysAgo);
                intent.putExtra("yesterday", Yesterday);

                intent.putExtra("status", messageArrayList.get(position).getStatus());
                context.startActivity(intent);
                ((Activity)context).finish();

            }
        });

    }

    @Override
    public int getItemCount() {
        if (messageArrayListFiltered.size() < 20) {
            return messageArrayListFiltered.size();
        } else {
            return count;
        }
    }

    public void setMyCount() {
        int remaining = messageArrayListFiltered.size() - count;
        if (remaining > 20) {
            this.count = count + 20;
        } else {
            this.count = count + remaining + 1;
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        } else {
            return MESSAGE_ITEM_VIEW_TYPE;
        }

    }

    private boolean isPositionFooter(int position) {
        return position == count - 1;
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout layoutRightButtons;
        private TextView id, message, title, date, time, iconText;

        CardView card_view;
        RelativeLayout relative_view;
        public ImageView iconImp;

        public MessageViewHolder(final View itemView) {
            super(itemView);
            layoutRightButtons = itemView.findViewById(R.id.layoutRightButtons);
            card_view = (CardView) itemView.findViewById(R.id.card_view);
            relative_view = (RelativeLayout) itemView.findViewById(R.id.relative_view);

            id = itemView.findViewById(R.id.id);
            title = itemView.findViewById(R.id.title);
            message = itemView.findViewById(R.id.message);
            date = itemView.findViewById(R.id.date_notify);
            time = itemView.findViewById(R.id.time_notify);

            btnArchive = itemView.findViewById(R.id.btnArchive);
            iconText = (TextView) itemView.findViewById(R.id.icon_text);
            iconImp = (ImageView) itemView.findViewById(R.id.icon_star1);

            Random r = new Random();
            int red = r.nextInt(255 - 0 + 1) + 0;
            int green = r.nextInt(255 - 0 + 1) + 0;
            int blue = r.nextInt(255 - 0 + 1) + 0;

            GradientDrawable draw = new GradientDrawable();
            draw.setShape(GradientDrawable.OVAL);
            draw.setColor(Color.rgb(red, green, blue));
            iconText.setBackground(draw);


            //Long Press
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    Intent intent = new Intent(context, RecylerDeleteAll.class);
                    context.startActivity(intent);

                    return false;
                }
            });

        }


    }


    public void delete(int position) {
        messageArrayListFiltered.remove(position);
        notifyItemRemoved(position);
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    messageArrayListFiltered = messageArrayList;
                } else {
                    ArrayList<Message> filteredList = new ArrayList<>();
                    for (Message row : messageArrayList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getTitle().toLowerCase().contains(charString.toLowerCase()) || row.getMessage().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    messageArrayListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = messageArrayListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                messageArrayListFiltered = (ArrayList<Message>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    class FooterViewHolder extends RecyclerView.ViewHolder {

        TextView footerText;

        public FooterViewHolder(View view) {
            super(view);
            footerText = (TextView) view.findViewById(R.id.footer_text);
        }

    }


    public void Alert(final int position) {
        new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Attention Please !")
                .setMessage("Are you sure you want to delete this message!")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        delete(position);

                        Toast.makeText(context, "Deleted!", Toast.LENGTH_LONG).show();

                    }

                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                }).show();
    }
}
