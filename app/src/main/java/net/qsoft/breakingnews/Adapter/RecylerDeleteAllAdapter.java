package net.qsoft.breakingnews.Adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.qsoft.breakingnews.app.AppController;
import net.qsoft.breakingnews.Database.DAO;
import net.qsoft.breakingnews.Model.Message;
import net.qsoft.breakingnews.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.StringTokenizer;


public class RecylerDeleteAllAdapter extends RecyclerView.Adapter<RecylerDeleteAllAdapter.ViewHolder> {

    private static String local_id;

    private Context context;
    private ArrayList<Message> messageArrayList;

    private SparseBooleanArray selectedItems;
    private int lastPosition = -1;

    public RecylerDeleteAllAdapter(Context context, ArrayList<Message> messageArrayList) {

        this.context = context;
        this.messageArrayList = messageArrayList;
    }

    @Override
    public RecylerDeleteAllAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyler_all_delete_item, null);

        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final RecylerDeleteAllAdapter.ViewHolder holder, int position) {

        final int pos = position;

        final Message messages = (Message) messageArrayList.get(position);
        DAO dao = new DAO(AppController.getInstance());
        dao.open();
        final int status = messages.getStatus();
        final String fv_id = messages.getId();
        final String fv_title = messages.getTitle();
        final String fv_message = messages.getMessage();
        final int flag = messages.getFlag();
        final int favourite = messages.getFavourite();
        final String category = messages.getCategory();
        final int flag1 = messages.getFlag();
        final String time = messages.getTime();

        StringTokenizer tokens = new StringTokenizer(time, ";");
        final String datePart = tokens.nextToken().trim();//
        final String timePart = tokens.nextToken().trim();//


        StringTokenizer tokens1 = new StringTokenizer(datePart, ",");
        final String WeekName = tokens1.nextToken().trim();//
        final String DPart2 = tokens1.nextToken().trim();//


        StringTokenizer tokens2 = new StringTokenizer(datePart, " ");
        final String P1 = tokens2.nextToken().trim();//
        final String P2 = tokens2.nextToken().trim();//

        SimpleDateFormat format = new SimpleDateFormat("EEEE", Locale.US);
        final String Today = format.format(new Date());


        String sDate = Today;
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE", Locale.getDefault());
        Date date1 = null;
        try {
            date1 = dateFormat.parse(sDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date1);
        calendar.add(Calendar.DATE, -1);
        final String Yesterday = dateFormat.format(calendar.getTime());


        if (WeekName.equals(Today)) {
            holder.time.setText(timePart);
        } else {
            if (P2.length() == 4) {
                holder.time.setText(DPart2.substring(0, 9));
            } else {
                holder.time.setText(DPart2.substring(0, 8));
            }
        }


        holder.title1.setText(messageArrayList.get(position).getTitle());
        holder.message1.setText(messageArrayList.get(position).getMessage());
        holder.iconText.setText(messageArrayList.get(position).getTitle().substring(0, 1));

        if (fv_title.length() >= 25) {
            holder.title1.setText(messages.getTitle().substring(0, 25) + "...");
        } else {
            holder.title1.setText(messages.getTitle());
        }
        if (fv_message.length() >= 32) {
            holder.message1.setText(messages.getMessage().substring(0, 32) + "...");
        }


        holder.chkSelected.setChecked(messageArrayList.get(position).isSelected());

        holder.chkSelected.setTag(messageArrayList.get(position));

        holder.chkSelected.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                Message model = (Message) cb.getTag();

                model.setSelected(cb.isChecked());
                messageArrayList.get(pos).setSelected(cb.isChecked());

            }
        });

        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                deleteItemFromList(v, pos);
                // holder.itemView.setVisibility(View.GONE);

            }
        });

    }


    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        return messageArrayList.size();
    }

    // confirmation dialog box to delete an unit
    private void deleteItemFromList(View v, final int position) {

        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());

        //builder.setTitle("Dlete ");
        builder.setMessage("Delete Item ?")
                .setCancelable(false)
                .setPositiveButton("CONFIRM",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                delete(position);

                                //messageArrayList.remove(position);
                                //notifyDataSetChanged();

                            }
                        })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                    }
                });

        builder.show();

    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageButton btn_delete;
        public CheckBox chkSelected;

        private LinearLayout layoutRightButtons;
        private TextView id, message1, title1, date, time, iconText;

        CardView card_view;
        RelativeLayout relative_view;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            layoutRightButtons = itemView.findViewById(R.id.layoutRightButtons);
            relative_view = (RelativeLayout) itemLayoutView.findViewById(R.id.relative_view);

            title1 = (TextView) itemLayoutView.findViewById(R.id.id_title);
            time = (TextView) itemLayoutView.findViewById(R.id.time1);
            message1 = (TextView) itemLayoutView.findViewById(R.id.message1);
            iconText = (TextView) itemView.findViewById(R.id.icon_text1);

            Random r = new Random();
            int red = r.nextInt(255 + 1);
            int green = r.nextInt(255 + 1);
            int blue = r.nextInt(255 + 1);

            GradientDrawable draw = new GradientDrawable();
            draw.setShape(GradientDrawable.OVAL);
            draw.setColor(Color.rgb(red, green, blue));
            iconText.setBackground(draw);

            btn_delete = (ImageButton) itemLayoutView.findViewById(R.id.btn_delete_unit);
            chkSelected = (CheckBox) itemLayoutView.findViewById(R.id.chk_selected);

        }
    }

    public void delete(int position) {
        final Message messages = (Message) messageArrayList.get(position);
        DAO dao = new DAO(AppController.getInstance());
        dao.open();
        local_id = messages.getId();
        dao.deleteSingleMessage(local_id);
        messageArrayList.remove(position);
        notifyItemRemoved(position);
        dao.close();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items =
                new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }
}