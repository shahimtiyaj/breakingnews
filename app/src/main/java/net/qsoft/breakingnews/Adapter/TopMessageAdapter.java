package net.qsoft.breakingnews.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.qsoft.breakingnews.Model.Messages;
import net.qsoft.breakingnews.R;

import java.util.ArrayList;

public class TopMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;


    private Context context;
    private ArrayList<Messages> messages=new ArrayList<Messages>();

    int count = 11;

    public TopMessageAdapter(Context context, ArrayList<Messages> messages) {
        this.context = context;
        this.messages = messages;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.test, parent, false);
            return new MyViewHolder(itemView);
        } else if (viewType == TYPE_FOOTER) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.footer_layout, parent, false);
            return new FooterViewHolder(itemView);
        } else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof MyViewHolder) {
            MyViewHolder myViewHolder = (MyViewHolder) holder;
            myViewHolder.icon_text.setText(messages.get(position).getTitle().substring(0, 1));
            myViewHolder.title.setText(messages.get(position).getTitle());
            myViewHolder.message.setText(messages.get(position).getMessage());
        } else if (holder instanceof FooterViewHolder) {
            FooterViewHolder footerHolder = (FooterViewHolder) holder;

            if ((count - 1) == messages.size()) {
                footerHolder.footerText.setText("NO MORE RESULTS TO SHOW");
            } else {
                footerHolder.footerText.setText("LOAD MORE");
            }

            footerHolder.footerText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setMyCount();
                    notifyDataSetChanged();
                }
            });
        }

    }


    @Override
    public int getItemCount() {
        if (messages.size() < 10) {
            return messages.size();
        } else {
            return count;
        }

    }

    public void setMyCount() {
        int remaining = messages.size() - count;
        if (remaining > 10) {
            this.count = count + 10;
        } else {
            this.count = count + remaining + 1;
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        } else {
            return TYPE_ITEM;
        }
    }

    private boolean isPositionFooter(int position) {
        return position == count - 1;
    }



}

class MyViewHolder extends RecyclerView.ViewHolder {

    ImageView image;
    TextView title;
    TextView message;
    TextView icon_text;


    MyViewHolder(View itemView) {
        super(itemView);

        icon_text = (TextView) itemView.findViewById(R.id.icon_text);
        title = (TextView) itemView.findViewById(R.id.title);
        message = (TextView) itemView.findViewById(R.id.message);


    }

}


class FooterViewHolder extends RecyclerView.ViewHolder {

    TextView footerText;

    public FooterViewHolder(View view) {
        super(view);
        footerText = (TextView) view.findViewById(R.id.footer_text);
    }
}