package net.qsoft.breakingnews.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import net.qsoft.breakingnews.Activity.CategoryDecoratedActivity;
import net.qsoft.breakingnews.Activity.DetailsActivity;
import net.qsoft.breakingnews.Activity.MessageActivity;
import net.qsoft.breakingnews.Activity.RecylerDeleteAll;
import net.qsoft.breakingnews.Database.DAO;
import net.qsoft.breakingnews.Model.Message;
import net.qsoft.breakingnews.R;
import net.qsoft.breakingnews.app.AppController;

import java.util.ArrayList;
import java.util.Random;

import static net.qsoft.breakingnews.Database.DBHelper.TABLE_USERINFO;

public class CategoryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable{
    private Context context;
    private ArrayList<Message> categoryItemArrayList;
    private ArrayList<Message> categoryItemListFiltered;

    private static final int MESSAGE_ITEM_VIEW_TYPE = 1;
    private static final int AD_VIEW_TYPE = 1;
    public int f = 0;
    private int count = 21;
    private static final int TYPE_FOOTER = 2;


    public CategoryListAdapter(Context context, ArrayList<Message> categoryItemArrayList) {
        this.context = context;
        this.categoryItemArrayList = categoryItemArrayList;
        this.categoryItemListFiltered = categoryItemArrayList;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == MESSAGE_ITEM_VIEW_TYPE) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.test, parent, false);
            return new CategoryListAdapter.MessageViewHolder(itemView);
        } else if (viewType == TYPE_FOOTER) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.footer_layout, parent, false);
            return new CategoryListAdapter.FooterViewHolder(itemView);
        } else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof CategoryListAdapter.MessageViewHolder) {

            CategoryListAdapter.MessageViewHolder messageViewHolder = (CategoryListAdapter.MessageViewHolder) holder;
            setMessageView(messageViewHolder, position);

        } else if (holder instanceof CategoryListAdapter.FooterViewHolder) {
            CategoryListAdapter.FooterViewHolder footerHolder = (CategoryListAdapter.FooterViewHolder) holder;

            if ((count - 1) == categoryItemArrayList.size()) {

                footerHolder.footerText.setText("NO MORE RESULTS TO SHOW");
            } else {

                footerHolder.footerText.setText("LOAD MORE");
            }

            footerHolder.footerText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setMyCount();
                    notifyDataSetChanged();
                }
            });

        }
    }

    @SuppressLint("SetTextI18n")
    private void setMessageView(final CategoryListAdapter.MessageViewHolder holder, final int position) {
        final Message categoryItem = (Message) categoryItemArrayList.get(position);
        DAO dao = new DAO(AppController.getInstance());
        dao.open();
        final int status = categoryItem.getStatus();
        final String fv_id = categoryItem.getId();
        final String fv_title = categoryItem.getTitle();
        final String fv_message = categoryItem.getMessage();
        final int flag = categoryItem.getFlag();
        final int favourite = categoryItem.getFavourite();
        final String category=categoryItem.getCategory();


        holder.categoryName.setText(categoryItem.getCategory());
        holder.title.setText(categoryItem.getTitle());

        holder.iconText.setText(categoryItem.getCategory().substring(0, 1));
        if (fv_title.length()>=28) {
            holder.title.setText(categoryItem.getTitle().substring(0,28)+"...");
        }
        else {
            holder.title.setText(categoryItem.getTitle());
        }

        Animation animation = AnimationUtils.loadAnimation(context, R.anim.translate);
        animation.setStartOffset(30 * position);//Provide delay here
        holder.itemView.startAnimation(animation);

        if (status == 0) {
            holder.categoryName.setTypeface(holder.categoryName.getTypeface(), android.graphics.Typeface.BOLD);

        }
        if (favourite == 0) {

            holder.iconImp.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_border_black_24dp));

        } else {

            holder.iconImp.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_black_24dp));

        }


        holder.iconImp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DAO dao = new DAO(AppController.getInstance());
                dao.open();
                dao.UpdateFlag(1, fv_id);

                if (favourite == 0) {
                    holder.iconImp.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_black_24dp));
                    dao.UpdateFavourite(1, fv_id);
                    Toast.makeText(context,  "Added to favourite ", Toast.LENGTH_LONG).show();

                } else {
                    holder.iconImp.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_border_black_24dp));
                    dao.UpdateFavourite(0, fv_id);
                    dao.UpdateFlag(0, fv_id);
                    categoryItemArrayList.remove(position);
                    notifyDataSetChanged();

                    Toast.makeText(context, "Remove from favourite ", Toast.LENGTH_LONG).show();
                }
            }
        });


        holder.relative_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (categoryItem.getCategory().equals(category)){

                  //  Toast.makeText(context, category, Toast.LENGTH_LONG).show();

                    DAO.executeSQL("INSERT OR REPLACE INTO " + TABLE_USERINFO +
                            "(FbProfileName, FbId, GmailProfileName, Gmail, Ucategory, fbphotoUrl) " +
                            "VALUES(?, ?, ?, ?, ?, ?)", new String[]{"", "", " ", " ", category, ""});


                    Intent intent = new Intent(context, CategoryDecoratedActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("id", categoryItemArrayList.get(position).getId());
                    intent.putExtra("category", categoryItemArrayList.get(position).getCategory());
                    intent.putExtra("title", categoryItemArrayList.get(position).getTitle());
                    intent.putExtra("status", categoryItemArrayList.get(position).getStatus());
                    context.startActivity(intent);
                }


                else {

                    Toast.makeText(context, "Category Not found!", Toast.LENGTH_LONG).show();

                }


            }
        });

    }

    @Override
    public int getItemCount() {
        if (categoryItemArrayList.size() < 20) {
            return categoryItemArrayList.size();
        } else {
            return count;
        }
    }

    public void setMyCount() {
        int remaining = categoryItemArrayList.size() - count;
        if (remaining > 20) {
            this.count = count + 20;
        } else {
            this.count = count + remaining + 1;
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        } else {
            return MESSAGE_ITEM_VIEW_TYPE;
        }

    }

    private boolean isPositionFooter(int position) {
        return position == count - 1;
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout layoutRightButtons;
        private TextView id, title, categoryName, date, time, iconText;

        CardView card_view;
        RelativeLayout relative_view;
        public ImageView iconImp;

        public MessageViewHolder(final View itemView) {
            super(itemView);
            layoutRightButtons = itemView.findViewById(R.id.layoutRightButtons);
            card_view = (CardView) itemView.findViewById(R.id.card_view);
            relative_view = (RelativeLayout) itemView.findViewById(R.id.relative_view);

            id = itemView.findViewById(R.id.id);
            categoryName = itemView.findViewById(R.id.title);
            title = itemView.findViewById(R.id.message);
            iconText = (TextView) itemView.findViewById(R.id.icon_text);
            iconImp = (ImageView) itemView.findViewById(R.id.icon_star1);

            //  mBadge = itemView.findViewById(R.id.badge);

            Random r = new Random();
            int red=r.nextInt(255 - 0 + 1)+0;
            int green=r.nextInt(255 - 0 + 1)+0;
            int blue=r.nextInt(255 - 0 + 1)+0;

            GradientDrawable draw = new GradientDrawable();
            draw.setShape(GradientDrawable.OVAL);
            draw.setColor(Color.rgb(red,green,blue));
            iconText.setBackground(draw);
            //Long Press
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    Intent intent = new Intent(context, RecylerDeleteAll.class);
                    context.startActivity(intent);

                    return false;
                }
            });

        }

    }


    public void delete(int position) {
        categoryItemArrayList.remove(position);
        notifyItemRemoved(position);
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    categoryItemArrayList = categoryItemArrayList;
                } else {
                    ArrayList<Message> filteredList = new ArrayList<>();
                    for (Message row : categoryItemArrayList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getTitle().toLowerCase().contains(charString.toLowerCase()) || row.getMessage().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    categoryItemArrayList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = categoryItemArrayList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                categoryItemArrayList = (ArrayList<Message>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    class FooterViewHolder extends RecyclerView.ViewHolder {

        TextView footerText;

        public FooterViewHolder(View view) {
            super(view);
            footerText = (TextView) view.findViewById(R.id.footer_text);
        }
    }
}
