package net.qsoft.breakingnews.Adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import net.qsoft.breakingnews.Database.DAO;
import net.qsoft.breakingnews.Model.Category;
import net.qsoft.breakingnews.R;
import net.qsoft.breakingnews.app.AppController;

import java.util.ArrayList;

public class CategorySelectAdapter extends BaseAdapter {
    String[] result;
    private ArrayList<Category> categoryArrayList;
    Context context;
    private static LayoutInflater inflater = null;

    public CategorySelectAdapter(Context context, ArrayList<Category> categoryArrayList) {
        // TODO Auto-generated constructor stub
        this.categoryArrayList = categoryArrayList;
        this.context = context;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return categoryArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        TextView category;
        CheckBox checkBox;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final Holder holder = new Holder();

        final Category category = (Category) categoryArrayList.get(position);
        final DAO dao = new DAO(AppController.getInstance());
        dao.open();
        final String categoryId = category.getId();
        final String categoryType = category.getCategory();
        final int Cflag = category.getCflag();

        View rowView;
        rowView = inflater.inflate(R.layout.content_category_list_item1, null);
        holder.category = (TextView) rowView.findViewById(R.id.category);
        holder.checkBox = (CheckBox) rowView.findViewById(R.id.checkbox);
        holder.category.setText(categoryArrayList.get(position).getCategory());
        if (Cflag == 0) {
            holder.checkBox.setChecked(false);
        } else {
            holder.checkBox.setChecked(true);

        }

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = preferences.edit();
        //  holder.checkBox.setChecked(preferences.getBoolean("checked",false));

        holder.checkBox.setOnClickListener(new View.OnClickListener() {

                                               @Override

                                               public void onClick(View v) {

                                                   if (Cflag == 0) {
                                                       dao.UpdateSingleCategoryFlag(1, categoryId);
                                                   } else {
                                                       dao.UpdateSingleCategoryFlag(0, categoryId);
                                                   }

//                holder.checkBoxL.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//
////                    if (Cflag==0){
////                            dao.UpdateSingleCategoryFlag(1,categoryId);
////                        }
////                        else {
////                            dao.UpdateSingleCategoryFlag(0,categoryId);
////                        }
//
//                    @Override
//                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//
//
//                        if (isChecked) {
//                          //  DAO dao = new DAO(AppController.getInstance());
//                           // dao.open();
//                            dao.UpdateSingleCategoryFlag(1, categoryId);
//
//                            // editor.putBoolean("checked", false);
//                            // editor.apply();
//
//                           // Toast.makeText(context, "Category ID:" + categoryId + ", Updated type: " + Cflag, Toast.LENGTH_LONG).show();
//
//                        } else {
//
//                            dao.UpdateSingleCategoryFlag(0, categoryId);
//
//                           // dao.close();
//
//                            // editor.putBoolean("checked", false);
//                            // editor.apply();
//
//                            //  DAO dao = new DAO(AppController.getInstance());
//                            //  dao.open();
//                            // dao.deleteSingleCategory(categoryId);
//
//                            //Toast.makeText(context, "Deselected !" + Cflag, Toast.LENGTH_LONG).show();
//
//                        }
//
//                    }
//
//
//
//                });


//                switch (position) {
//
//                    case 0:
//
//                        Toast.makeText(context, "Category ID:" + categoryId+", Category type: "+categoryType, Toast.LENGTH_LONG).show();
//
//                        break;
//                    case 1:
//                        Toast.makeText(context, "Category ID:" + categoryId+", Category type: "+categoryType, Toast.LENGTH_LONG).show();
//
//                      //  Toast.makeText(context, "Position:" + position, Toast.LENGTH_LONG).show();
//
//                        break;
//
//                    default:
//                        break;
//
//                }


                /*Intent intent = new Intent(context, MainActivity.class);
                context.startActivity(intent);
                ((Activity) context).overridePendingTransition(R.anim.left_in, R.anim.left_out);
                ((Activity) context).finish();*/
                                               }
                                           }

        );

        return rowView;
    }


}