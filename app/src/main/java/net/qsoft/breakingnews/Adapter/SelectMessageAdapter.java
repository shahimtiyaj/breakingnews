package net.qsoft.breakingnews.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.qsoft.breakingnews.app.AppController;
import net.qsoft.breakingnews.Database.DAO;
import net.qsoft.breakingnews.Model.Message;
import net.qsoft.breakingnews.R;

import java.util.ArrayList;


public class SelectMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private Context context;
    private ArrayList<Message> messageArrayList;
    private ArrayList<Message> messageArrayListFiltered;
    private SelectMessageAdapter.messageAdapterListener listener;
    public Button btnArchive;

    private static final int MESSAGE_ITEM_VIEW_TYPE = 1;
    private static final int AD_VIEW_TYPE = 1;
    private static String local_id;
    public int f = 0;

    private int count = 11;
    private static final int TYPE_FOOTER = 2;


    //nex3z notification  badge --
    public com.nex3z.notificationbadge.NotificationBadge mBadge;
    public int mCount = 1;

    TextView smsCountTxt;
    int pendingSMSCount = 10;

    public SelectMessageAdapter(Context context, ArrayList<Message> messageArrayList, messageAdapterListener listener) {
        this.context = context;
        this.messageArrayList = messageArrayList;
        this.listener = listener;
        this.messageArrayListFiltered = messageArrayList;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == MESSAGE_ITEM_VIEW_TYPE) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.select_message_delete_layout, parent, false);
            return new SelectMessageAdapter.MessageViewHolder(itemView);
        } else if (viewType == TYPE_FOOTER) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.footer_layout, parent, false);
            return new SelectMessageAdapter.FooterViewHolder(itemView);
        } else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof MessageViewHolder) {

            MessageViewHolder messageViewHolder = (MessageViewHolder) holder;
            setMessageView(messageViewHolder, position);

        } else if (holder instanceof MessageListAdapter.FooterViewHolder) {
            MessageListAdapter.FooterViewHolder footerHolder = (MessageListAdapter.FooterViewHolder) holder;

            if ((count - 1) == messageArrayListFiltered.size()) {
                footerHolder.footerText.setText("NO MORE RESULTS TO SHOW");
            } else {
                footerHolder.footerText.setText("LOAD MORE");
            }

            footerHolder.footerText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setMyCount();
                    notifyDataSetChanged();
                }
            });


        }
    }

    private void setMessageView(final MessageViewHolder holder, final int position) {
        final Message messages = (Message) messageArrayListFiltered.get(position);
        DAO dao = new DAO(AppController.getInstance());
        dao.open();
        final int status = messages.getStatus();
        final String fv_id = messages.getId();
        final String fv_title = messages.getTitle();
        final String fv_message = messages.getMessage();
        final int flag = messages.getFlag();

        holder.title.setText(messages.getTitle());
        holder.message.setText(messages.getMessage());
        holder.iconText.setText(messages.getTitle().substring(0, 1));
    }


    @Override
    public int getItemCount() {
        if (messageArrayListFiltered.size() < 10) {
            return messageArrayListFiltered.size();
        } else {
            return count;
        }
    }

    public void setMyCount() {
        int remaining = messageArrayListFiltered.size() - count;
        if (remaining > 10) {
            this.count = count + 10;
        } else {
            this.count = count + remaining + 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        } else {
            return MESSAGE_ITEM_VIEW_TYPE;
        }
    }

    private boolean isPositionFooter(int position) {
        return position == count - 1;
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout layoutRightButtons;
        TextView id, message, title, date, time, iconText;

        CheckBox checkBox;

        CardView card_view;
        RelativeLayout relative_view;

        public MessageViewHolder(View itemView) {
            super(itemView);
            layoutRightButtons = itemView.findViewById(R.id.layoutRightButtons);
            card_view = (CardView) itemView.findViewById(R.id.card_view);
            relative_view = (RelativeLayout) itemView.findViewById(R.id.relative_view);

            id = itemView.findViewById(R.id.id);
            title = itemView.findViewById(R.id.title);
            message = itemView.findViewById(R.id.message);
            checkBox = itemView.findViewById(R.id.checkbox);

            btnArchive = itemView.findViewById(R.id.btnArchive);
            iconText = (TextView) itemView.findViewById(R.id.icon_text);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                }
            });

        }

    }


    public void delete(int position) {
        final Message messages = (Message) messageArrayListFiltered.get(position);
        DAO dao = new DAO(AppController.getInstance());
        dao.open();
        local_id = messages.getId();
        dao.deleteSingleMessage(local_id);
        dao.close();
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    messageArrayListFiltered = messageArrayList;
                } else {
                    ArrayList<Message> filteredList = new ArrayList<>();
                    for (Message row : messageArrayList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getTitle().toLowerCase().contains(charString.toLowerCase()) || row.getMessage().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    messageArrayListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = messageArrayListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                messageArrayListFiltered = (ArrayList<Message>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface messageAdapterListener {
        void onMessageSelected(Message message);
    }


    class FooterViewHolder extends RecyclerView.ViewHolder {
        TextView footerText;

        public FooterViewHolder(View view) {
            super(view);
            footerText = (TextView) view.findViewById(R.id.footer_text);
        }
    }


    public void Alert(final int position) {
        new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Attention Please !")
                .setMessage("Are you sure you want to delete this message!")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        delete(position);

                        Toast.makeText(context, "Deleted!", Toast.LENGTH_LONG).show();

                    }

                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                }).show();
    }


}
