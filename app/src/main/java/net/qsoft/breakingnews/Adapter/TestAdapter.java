package net.qsoft.breakingnews.Adapter;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.qsoft.breakingnews.Model.Message;
import net.qsoft.breakingnews.R;

import java.util.ArrayList;
import java.util.List;

public class TestAdapter extends ArrayAdapter<Message> {

    private ArrayList<Message> list;
    private LayoutInflater inflator;

    public TestAdapter(Activity context, ArrayList<Message> list) {
        super(context, R.layout.select_message_delete_layout, list);
        this.list = list;
        inflator = context.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if (convertView == null) {
            convertView = inflator.inflate(R.layout.select_message_delete_layout, null);
            holder = new ViewHolder();

            holder.id = (TextView) convertView.findViewById(R.id.id);
            holder.title = (TextView) convertView.findViewById(R.id.title);

            holder.message = (TextView) convertView.findViewById(R.id.message);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkbox);

            holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton view,
                                             boolean isChecked) {
                    int getPosition = (Integer) view.getTag();
                    list.get(getPosition).setSelected(view.isChecked());
                }
            });
            convertView.setTag(holder);
            convertView.setTag(R.id.title, holder.title);
            convertView.setTag(R.id.message, holder.message);
            convertView.setTag(R.id.checkbox, holder.checkBox);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.checkBox.setTag(position);

        holder.id.setText(list.get(position).getId());
        holder.checkBox.setChecked(list.get(position).isSelected());

        return convertView;
    }

    static class ViewHolder {
        protected LinearLayout layoutRightButtons;
        protected TextView id, message, title, date, time, iconText;
        protected CheckBox checkBox;
        protected CardView card_view;
        protected RelativeLayout relative_view;
    }
}
