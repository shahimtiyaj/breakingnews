package net.qsoft.breakingnews.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.qsoft.breakingnews.R;


/**
 * Created by Md. Imtiyaj on 22-Feb-18.
 */

public class FragmentAboutUs extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.fragment_about, null);

        String versionName = "";
        try {
            versionName = getActivity().getPackageManager().getPackageInfo(getContext().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        TextView versionname = (TextView)v.findViewById(R.id.version_id);
        versionname.setText(versionName);


        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("back", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("now","dev");
        editor.apply();
        return v;
    }
}