package net.qsoft.breakingnews.Fragment;

import android.app.SearchManager;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import net.qsoft.breakingnews.Adapter.CategoryListAdapter;
import net.qsoft.breakingnews.Adapter.MessageListAdapter;
import net.qsoft.breakingnews.Adapter.SectionListDataAdapter;
import net.qsoft.breakingnews.app.AppController;
import net.qsoft.breakingnews.Database.DAO;
import net.qsoft.breakingnews.Model.Message;
import net.qsoft.breakingnews.R;
import net.qsoft.breakingnews.helper.DividerItemDecoration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Q-soft on 18/2/2018.
 */

public class FragmentAllMessageList extends Fragment{

    private LinearLayoutManager layoutManager;
     MessageListAdapter adapter;
    //CategoryListAdapter adapter;

    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout swipeRefresh;
    ArrayList<Message> messagesArrayList;

    private RelativeLayout mainLayout, emptyLayout;
    private SearchView searchView;

    public FragmentAllMessageList() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recyler_home, container, false);
       // whiteNotificationBar(view);

        messagesArrayList = new ArrayList<>();
        adapter = new MessageListAdapter(getActivity(), messagesArrayList);

      //  Toast.makeText(getContext(),DateTime(),Toast.LENGTH_LONG).show();


        mRecyclerView = view.findViewById(R.id.product_list_recycler_view);
        swipeRefresh = view.findViewById(R.id.swipe_refresh_layout);
        mainLayout = view.findViewById(R.id.main_layout);
        emptyLayout = view.findViewById(R.id.empty_layout);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimaryDark));
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadMessageList();
            }
        });
        loadMessageList();
        setHasOptionsMenu(true);
        return view;
    }

    private void loadMessageList() {
        DAO dao = new DAO(AppController.getInstance());
        dao.open();
        messagesArrayList=dao.getAllMessage();

        if (!messagesArrayList.isEmpty()) {
            adapter = new MessageListAdapter(getContext(), messagesArrayList);
            mRecyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            swipeRefresh.setRefreshing(false);
            emptyLayout.setVisibility(View.GONE);
        } else {
            emptyLayout.setVisibility(View.VISIBLE);
            swipeRefresh.setRefreshing(false);
        }
        dao.close();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu,inflater);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager)  getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo( getActivity().getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                adapter.getFilter().filter(query);
                return false;
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_search) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void whiteNotificationBar(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = view.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            view.setSystemUiVisibility(flags);
            getActivity().getWindow().setStatusBarColor(Color.WHITE);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

    }

    public String LastDownloadDateTime() {
        String myDate="Sunday, 8th July 2018";
        SimpleDateFormat src = new SimpleDateFormat("EEEE, d'th' MMMM yyyy", Locale.ENGLISH);

        SimpleDateFormat dt = new SimpleDateFormat("EEEE, MMM d, yyyy", Locale.ENGLISH);
        Date date = null;

        try {
            date = src.parse(myDate);
        } catch (ParseException e) {
            Log.d("Exception",e.getMessage());
        }

       //  String result = dt.format(date);
       // Log.d("result", result);

        return dt.format(date);

        // return DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(date);
        // return DateFormat.getDateTimeInstance().format(new Date());//latest and standard formt
    }

    public String DateTime() {
        //SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat dt = new SimpleDateFormat("EEEE, d'th' MMMM yyyy", Locale.ENGLISH);

        Date date = new Date();
        return dt.format(date);
        // return DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(date);
        // return DateFormat.getDateTimeInstance().format(new Date());//latest and standard formt
    }


}
